#/bin/bash

PSQL="psql -At -h radiatordb -U radiator radiatordb "
CLONE_TO_DIR="src"
export PGPASSWORD=radiator
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.9.11-9.fc32.x86_64/



# params: project_id
get_repository() {
	URL=`$PSQL -c "select url from project where id=$id"`;	
	PRIV_KEY=`$PSQL -c "select private_key from project where id=$id"`;	
		
#	echo $id;
#	echo $URL;
#	echo $PRIV_KEY;

	rm -rf $id 2>/dev/null
	mkdir $id;
	cd $id;
	
	if [[ $URL == "git@"* ]] ; then
		(
			echo "-----BEGIN RSA PRIVATE KEY-----";
			echo $PRIV_KEY;
		        echo "-----END RSA PRIVATE KEY-----"
		)> priv.key
		chmod 600 priv.key
		ssh-agent bash -c "ssh-add ./priv.key; git clone $URL $CLONE_TO_DIR"

	elif [[ $URL == "http"* ]] ; then
		git clone $URL $CLONE_TO_DIR
	fi

	cd ..
	
}

# params: project_id
run_tests()
{
	cloc_test $1

	git_lines_test $1

	if [ -f $1/$CLONE_TO_DIR/pom.xml ] ; then
                find_bugs $1
                junit_tests $1
		jacoco $1
	fi
	
}




junit_tests()
{
	echo JUNIT TESTS
        cd $1/$CLONE_TO_DIR
        RESULT=`mvn clean test |  grep "Tests run"`;
	insert_results $1 "$RESULT" JUNIT_TESTS
	cd ../..
}

find_bugs()
{
	echo FINDBUG
        cd $1/$CLONE_TO_DIR
        mvn  -DskipTests=true clean  compile
	/spotbugs/bin/spotbugs  -textui -progress -xml -outputFile fb.xml target/classes/
	
	RESULT=`cat fb.xml`
	
	insert_results $1 "$RESULT" FINDBUGS
	cd ../..
}

jacoco()
{
	echo JACOCO
        cd $1/$CLONE_TO_DIR
	mvn clean 
	mvn org.jacoco:jacoco-maven-plugin:0.8.6:prepare-agent test org.jacoco:jacoco-maven-plugin:0.8.6:report
	
	RESULT=`cat target/site/jacoco/jacoco.csv`
	
	insert_results $1 "$RESULT" JACOCO
	cd ../..
}




cloc_test()
{

	echo CLOC TESTS
	RESULT=`cloc $1/$CLONE_TO_DIR`
	insert_results $1 "$RESULT" "CLOC"

}


git_lines_test()
{
	echo GIT LINES TESTS
	cd $1/$CLONE_TO_DIR

	RESULT=`git log --all --since=1.day.ago --numstat | grep "^[0-9]"`
	
	insert_results $1 "$RESULT" "GIT_LINES"
	cd ../..
}

insert_results()
{
	SQL="INSERT INTO test_result(id, created, result, type, project_id) VALUES (nextval('test_result_seq'), now(), '$2', '$3', $1)"
	echo $SQL
	$PSQL -c "$SQL"  
}


while [ true ] ; do
	date;

	for id in `$PSQL -c "select id from project"`; do 


		echo =========================================== $id =============================
		get_repository $id ;

		run_tests $id


		rm -rf $id
	done


	sleep 3000;
done

