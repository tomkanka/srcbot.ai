package com.deviniti.radiator.metrics;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.model.TestResult;
import com.deviniti.radiator.model.TestResultType;

class GitLinesParserTest {
	
	private static final String REPORT = 
		"33      15      szop-client/src/main/java/com/intenso/vlom/client/modules/lawsuits/civilLawsuit/edit/LawsuitEditWindow.java\r\n" + 
		"44      11      szop-client/src/main/java/com/intenso/vlom/client/modules/lawsuits/details/form/EditGeneralLawsuitForm.java\r\n" + 
		"15      11      szop-client/src/main/java/com/intenso/vlom/client/modules/lawsuits/edit/RetentionWindow.java\r\n" + 
		"19      11      szop-server/src/main/java/com/intenso/vlom/dao/RetentionDAOImpl.java\r\n" + 
		"38      20      szop-client/src/main/java/com/intenso/vlom/client/modules/lawsuits/civilLawsuit/edit/LawsuitEditWindow.java\r\n" + 
		"7       2       szop-client/src/main/java/com/intenso/vlom/client/modules/lawsuits/edit/RetentionWindow.java\r\n" + 
		"18      2       szop-server/src/main/java/com/intenso/vlom/server/lawsuit/RetentionServiceImpl.java\r\n" + 
		"3       1       szop-shared/src/main/java/com/intenso/vlom/shared/service/RetentionService.java\r\n" + 
		"3       1       szop-shared/src/main/java/com/intenso/vlom/shared/service/RetentionServiceAsync.java\r\n" + 
		"1       1       pom.xml\r\n" + 
		"24      0       szop-client/src/main/java/com/intenso/vlom/client/modules/settlements/form/CivilCostDialog.java\r\n" + 
		"8       0       szop-shared/src/main/java/com/intenso/vlom/client/dto/DamageDTO.java";

	private GitLinesParser parser;
	private TestResult result;
	private List<Metric> allMetrics;
	private Metric metric;
	
	
	@BeforeEach
	public void before() {
		parser = new GitLinesParser();
		Project project = new Project();
		project.setSizeOfProject(12);
		project.setHash("project_hash");
		project.setDevelopers(2);
		
		result = new TestResult();
		result.setResult(REPORT);
		result.setCreated(new Date());
		result.setProject(project);
		result.setType(TestResultType.JUNIT_TESTS);
		
		allMetrics = new ArrayList<Metric>();
		
		Metric m1 = new Metric();
		m1.setProject(project);
		GregorianCalendar c1 = new GregorianCalendar();
		c1.add(GregorianCalendar.DAY_OF_MONTH, -7);
		m1.setCreated(c1.getTime());
		m1.setProject(project);
		m1.setLastAddedLines(47);
		m1.setLastDeletedLines(25);
		allMetrics.add(m1);
		
		Metric m2 = new Metric();
		m2.setProject(project);
		GregorianCalendar c2 = new GregorianCalendar();
		c2.add(GregorianCalendar.DAY_OF_MONTH, -9);
		m2.setCreated(c2.getTime());
		m2.setProject(project);
		m2.setLastAddedLines(40);
		m2.setLastDeletedLines(50);
		allMetrics.add(m2);
		
		metric = new Metric();
		metric.setProject(project);
		metric.setCreated(new Date());
	}

	@Test
	void parse_correctlyFilledLasstAddedAndRemovedLines() {
		// given

		// when
		parser.updateMetric(result, metric, allMetrics);

		// then
		assertThat(metric.getLastAddedLines()).isEqualTo(213);
		assertThat(metric.getLastDeletedLines()).isEqualTo(75);
	}
	
	@Test
	void parse_correctlyFilledAverageAddedAndRemovedLines() {
		// given
		BigDecimal averateAddedLines = new BigDecimal("50.00");
		BigDecimal averateDeletedLines = new BigDecimal("25.00");

		// when
		parser.updateMetric(result, metric, allMetrics);

		// then
		assertThat(metric.getAverageAddedLines()).isEqualTo(averateAddedLines);
		assertThat(metric.getAverageDeletedLines()).isEqualTo(averateDeletedLines);
	}
	
	@Test
	void parse_correctlyFilledTotalAverageAddedAndRemovedLines() {
		// given
		BigDecimal totalAverateAddedLines = new BigDecimal("50.00");
		BigDecimal totalAverateDeletedLines = new BigDecimal("25.00");

		// when
		parser.updateMetric(result, metric, allMetrics);

		// then
		assertThat(metric.getTotalAverageAddedLines()).isEqualTo(totalAverateAddedLines);
		assertThat(metric.getTotalAverageDeletedLines()).isEqualTo(totalAverateDeletedLines);
	}

}
