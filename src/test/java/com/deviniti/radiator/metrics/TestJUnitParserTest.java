package com.deviniti.radiator.metrics;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.model.TestResult;
import com.deviniti.radiator.model.TestResultType;

class TestJUnitParserTest {

	private static final String REPORT = 
			  "  3 | 2021-01-21 22:42:40.984162 | [INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.284 s - in org.springframework.samples.petclinic.model.ValidatorTests          +| TEST_COVERAGE |          2\r\n"
			+ "    |                            | [INFO] Tests run: 11, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 8.37 s - in org.springframework.samples.petclinic.service.ClinicServiceTests    +|               |\r\n"
			+ "    |                            | [WARNING] Tests run: 1, Failures: 0, Errors: 0, Skipped: 1, Time elapsed: 0.001 s - in org.springframework.samples.petclinic.system.CrashControllerTests+|               |\r\n"
			+ "    |                            | [INFO] Tests run: 3, Failures: 0, Errors: 1, Skipped: 0, Time elapsed: 4.436 s - in org.springframework.samples.petclinic.owner.VisitControllerTests    +|               |\r\n"
			+ "    |                            | [INFO] Tests run: 6, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.902 s - in org.springframework.samples.petclinic.owner.PetControllerTests      +|               |\r\n"
			+ "    |                            | [INFO] Tests run: 3, Failures: 1, Errors: 0, Skipped: 0, Time elapsed: 0.048 s - in org.springframework.samples.petclinic.owner.PetTypeFormatterTests   +|               |\r\n"
			+ "    |                            | [INFO] Tests run: 11, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.42 s - in org.springframework.samples.petclinic.owner.OwnerControllerTests    +|               |\r\n"
			+ "    |                            | [INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.323 s - in org.springframework.samples.petclinic.vet.VetControllerTests        +|               |\r\n"
			+ "    |                            | [INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.015 s - in org.springframework.samples.petclinic.vet.VetTests                  +|               |\r\n"
			+ "    |                            | [INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 4.311 s - in org.springframework.samples.petclinic.PetclinicIntegrationTests     +|               |\r\n"
			+ "    |                            | [WARNING] Tests run: 40, Failures: 1, Errors: 1, Skipped: 1                                                                                              |               |";

	private TestJUnitParser parser;
	private TestResult result;
	private List<Metric> allMetrics;
	private Metric metric;

	@BeforeEach
	public void before() {
		parser = new TestJUnitParser();
		Project project = new Project();
		project.setSizeOfProject(12);
		project.setHash("project_hash");
		project.setDevelopers(2);
		
		result = new TestResult();
		result.setResult(REPORT);
		result.setCreated(new Date());
		result.setProject(project);
		result.setType(TestResultType.JUNIT_TESTS);
		
		allMetrics = new ArrayList<Metric>();
		
		Metric m1 = new Metric();
		m1.setProject(project);
		GregorianCalendar c1 = new GregorianCalendar();
		c1.add(GregorianCalendar.DAY_OF_MONTH, -7);
		m1.setCreated(c1.getTime());
		m1.setProject(project);
		m1.setUnitTests(30);
		m1.setHeighErrors(10);
		m1.setLastUnitTests(10);
		m1.setLastHeighErrorsChange(10);
		allMetrics.add(m1);
		
		Metric m2 = new Metric();
		m2.setProject(project);
		GregorianCalendar c2 = new GregorianCalendar();
		c2.add(GregorianCalendar.DAY_OF_MONTH, -9);
		m2.setCreated(c2.getTime());
		m2.setProject(project);
		m2.setUnitTests(10);
		m2.setHeighErrors(1);
		m2.setLastUnitTests(10);
		m2.setLastHeighErrorsChange(1);
		allMetrics.add(m2);
		
		metric = new Metric();
		metric.setProject(project);
		metric.setCreated(new Date());
	}

	@Test
	void parseTestCoverage_correctlyFilledTestsAndHeighErrorsNumber() {
		// given

		// when
		parser.updateMetric(result, metric, allMetrics);

		// then
		assertThat(metric.getUnitTests()).isEqualTo(40);
		assertThat(metric.getHeighErrors()).isEqualTo(2);

	}

	@Test
	void arseTestCoverage_correctlyFilledLastTestsAndHeigErrorsChange() {
		// given

		// when
		parser.updateMetric(result, metric, allMetrics);

		// then
		assertThat(metric.getLastUnitTests()).isEqualTo(10);
		assertThat(metric.getLastHeighErrorsChange()).isEqualTo(-8);
	}
	
	@Test
	void arseTestCoverage_correctrlyFilledAverageTestsAndHeigErrors() {
		// given

		// when
		parser.updateMetric(result, metric, allMetrics);

		// then
		assertThat(metric.getAverageNewUnitTests()).isEqualTo(new BigDecimal("5.00"));
		assertThat(metric.getAverageHeighErrorsChange()).isEqualTo(new BigDecimal("0.50"));
	}

	@Test
	void arseTestCoverage_withoutAnyMetrics_correctrlyFilledAverageTestsAndHeigErrors() {
		// given

		// when
		parser.updateMetric(result, metric, new ArrayList<Metric>());

		// then
		assertThat(metric.getAverageNewUnitTests()).isEqualTo(new BigDecimal("20.00"));
		assertThat(metric.getAverageHeighErrorsChange()).isEqualTo(new BigDecimal("1.00"));
	}
	
	@Test
	void arseTestCoverage_correctrlyFilledTotalAverageTestsAndHeigErrors() {
		// given

		// when
		parser.updateMetric(result, metric, allMetrics);

		// then
		assertThat(metric.getTotalAverageNewUnitTests()).isEqualTo(new BigDecimal("5.00"));
		assertThat(metric.getTotalAverageHeighErrorsChange()).isEqualTo(new BigDecimal("0.50"));
	}
}
