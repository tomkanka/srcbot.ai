package com.deviniti.radiator.metrics;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.model.TestResult;
import com.deviniti.radiator.model.TestResultType;

class ClocParserTest {
	
	private static final String REPORT = 
			"      49 text files.                                                           \r\n" + 
			"classified 49 files\\r      44 unique files.                                    \r\n" + 
			"      27 files ignored.                                                        \r\n" + 
			"                                                                               \r\n" + 
			"github.com/AlDanial/cloc v 1.82  T=0.04 s (530.0 files/s, 23621.4 lines/s)     \r\n" + 
			"-------------------------------------------------------------------------------\r\n" + 
			"Language                     files          blank        comment           code\r\n" + 
			"-------------------------------------------------------------------------------\r\n" + 
			"Dart                             6             62             34            411\r\n" + 
			"JSON                             2              0              0            145\r\n" + 
			"Gradle                           3             18              3             84\r\n" + 
			"XML                              5             18             34             62\r\n" + 
			"Markdown                         3             14              0             36\r\n" + 
			"YAML                             1             16             47             21\r\n" + 
			"Swift                            1              1              0             12\r\n" + 
			"Kotlin                           1              2              0              4\r\n" + 
			"C/C++ Header                     1              0              0              1\r\n" + 
			"-------------------------------------------------------------------------------\r\n" + 
			"SUM:                            23            131            118            776\r\n" + 
			"-------------------------------------------------------------------------------\r\n" + 
			" ";
	
	private ClocParser parser;
	private TestResult result;
	private List<Metric> allMetrics;
	
	@BeforeEach
	public void before() {
		parser = new ClocParser();
		Project project = new Project();
		project.setSizeOfProject(12);
		project.setHash("project_hash");
		result = new TestResult();
		result.setResult(REPORT);
		result.setCreated(new Date());
		result.setProject(project);
		result.setType(TestResultType.CLOC);
		allMetrics = new ArrayList<Metric>();
		Metric m1 = new Metric();
		m1.setProject(project);
		GregorianCalendar c = new GregorianCalendar();
		c.add(GregorianCalendar.DAY_OF_MONTH, -7);
		m1.setCreated(c.getTime());
		m1.setProject(project);
		allMetrics.add(m1);
	}
	
	@Test
	void parseCloc_correctlyFilledEntity() {
		//given
		Metric metric = new Metric();
		
		//when
		parser.updateMetric(result, metric, allMetrics);
		
		//then
		assertThat(metric.getTotalLines()).isEqualTo(776);
	}
	
	
	@Test
	void parseToMetrics_projectWithoutMetrics_correctlyFilledEntity() {
		//given
		Metric metric = new Metric();
		List<Metric> allMetrics = new ArrayList<Metric>();
		
		//when
		parser.updateMetric(result, metric, allMetrics);
		
		//then
	}

}
