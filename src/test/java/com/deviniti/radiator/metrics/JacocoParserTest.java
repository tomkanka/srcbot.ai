package com.deviniti.radiator.metrics;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.model.TestResult;
import com.deviniti.radiator.model.TestResultType;

class JacocoParserTest {
	
	private static final String REPORT = 
			"GROUP,PACKAGE,CLASS,INSTRUCTION_MISSED,INSTRUCTION_COVERED,BRANCH_MISSED,BRANCH_COVERED,LINE_MISSED,LINE_COVERED,COMPLEXITY_MISSED,COMPLEXITY_COVERED,METHOD_MISSED,METHOD_COVERED\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.elzab,ElzabPromoDTO,17,0,0,0,7,0,5,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.elzab,ElzabModule,3038,0,128,0,437,0,92,0,28,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.elzab,ElzabProductDTO,59,0,0,0,25,0,17,0,17,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.elzab,FieldDescription.FIELD_TYPE,64,0,0,0,1,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.elzab,ElzabSTXModule,1921,0,166,0,430,0,114,0,31,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.elzab,FieldDescription,24,0,0,0,6,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.elzab,ElzabSTXNewModule,2143,0,202,0,478,0,134,0,33,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.elzab,ProductDbMapImpl,54,0,4,0,12,0,8,0,6,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.posnet,PosnetResultParser,420,0,38,0,78,0,28,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.posnet,PosnetCommand,340,0,0,0,30,0,3,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.posnet,PosnetCommandCreator,369,0,18,0,64,0,14,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.posnet,PosnetProductDTO,52,0,0,0,22,0,15,0,15,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.posnet,PosnetField,381,0,28,0,66,0,24,0,10,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.posnet,PosnetModule,3202,0,204,0,648,0,143,0,41,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.posnet,BCD,189,0,18,0,35,0,13,0,4,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionItemRequestDTO.PromotionItemRequestDTOBuilder,15,0,0,0,1,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionCreateDTO,159,0,24,0,7,0,24,0,12,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionDeviceResponseDTO,365,0,62,0,10,0,51,0,20,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionShopStateUpdateDTO.PromotionShopStateUpdateDTOBuilder,62,0,0,0,1,0,7,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionCreateDTO.PromotionCreateDTOBuilderImpl,7,0,0,0,1,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionItemResponseDTO,103,0,16,0,5,0,17,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionCreateDTO.PromotionCreateDTOBuilder,37,0,0,0,1,0,4,0,4,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionUpdateDTO.PromotionUpdateDTOBuilderImpl,7,0,0,0,1,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionItemResponseDTO.PromotionItemResponseDTOBuilderImpl,7,0,0,0,1,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionResponseDTO.PromotionResponseDTOBuilder,48,0,0,0,1,0,5,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionItemRequestDTO,49,0,8,0,4,0,11,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionItemRequestDTO.PromotionItemRequestDTOBuilderImpl,7,0,0,0,1,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionUpdateDTO,103,0,16,0,5,0,17,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionResponseDTO.PromotionResponseDTOBuilderImpl,7,0,0,0,1,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionShopStateUpdateDTO,231,0,38,0,8,0,34,0,15,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionItemResponseDTO.PromotionItemResponseDTOBuilder,26,0,0,0,1,0,3,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionShopResponseDTO,365,0,62,0,10,0,51,0,20,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionUpdateDTO.PromotionUpdateDTOBuilder,26,0,0,0,1,0,3,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion,PromotionResponseDTO,209,0,32,0,8,0,30,0,14,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app,AppStateComponent,196,0,4,0,26,0,9,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app,AppState,44,0,0,0,2,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app,SmartBoxAppPi,48,0,0,0,17,0,4,0,4,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app,DeviceSerial,26,0,4,0,10,0,4,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register,TcpIpCommunicationModule,615,0,76,0,150,0,52,0,14,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register,SerialCommunictionModule,768,0,98,0,199,0,68,0,19,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register,CashRegisterModuleFactory,310,0,52,0,74,0,33,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register,CashRegisterIOException,12,0,0,0,6,0,3,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register,CashRegisterModule.Capabilities,34,0,0,0,1,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register,Utils,113,38,14,4,27,9,11,3,3,2\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register,CashRegisterException,12,0,0,0,6,0,3,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task.register,TimeDiffTask,70,0,4,0,14,0,5,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task.register,ChangeProductExistsTask,58,0,6,0,14,0,5,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task.register,ReadSdCardTask,227,0,18,0,50,0,12,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task.register,ChangeProductNamesTask,371,0,34,0,51,0,20,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task.register,TestTask,39,0,2,0,7,0,3,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task.register,ReadTransactionsTask,292,0,30,0,66,0,21,0,6,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task.register,SetupPromoTask,892,0,92,0,161,0,53,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.checker,AbstractRegisterChecker,301,0,32,0,85,0,26,0,10,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.checker,ElzabSTXChecker,67,0,2,0,15,0,3,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.checker,ElzabChecker,196,0,2,0,27,0,3,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.checker,NovitusChecker,214,0,4,0,55,0,6,0,4,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.checker,PosnetChecker,138,0,10,0,31,0,8,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CheckoutPointRequestDTO.CheckoutPointRequestDTOBuilderImpl,7,0,0,0,1,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,AddressDTO,231,0,38,0,8,0,34,0,15,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,NotificationDTO,212,0,38,0,5,0,32,0,13,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,StatisticsResultDTO,231,0,38,0,8,0,34,0,15,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,DevicePointBindingDTO,126,0,22,0,3,0,20,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,FirebaseDataDTO.FirebaseDataDTOBuilder,38,0,0,0,1,0,5,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,UserDTO,126,0,22,0,3,0,20,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,TransactionReportedDTO,467,0,78,0,11,0,63,0,24,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ExternalJwtAuthenticationCompanyResponseDTO,113,0,18,0,4,0,18,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,JwtAuthenticationResponseDTO,272,0,50,0,8,0,39,0,14,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopWithPointsDTO.ShopWithPointsDTOBuilder,86,0,0,0,1,0,9,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,OpeningHoursDTO,323,0,54,0,10,0,46,0,19,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopPointsDTO.ShopPointsDTOBuilder,38,0,0,0,1,0,5,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ExternalJwtAuthenticationResponseContentDTO,543,0,98,0,14,0,78,0,29,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,PagingPropertiesDTO.PagingPropertiesDTOBuilder,92,0,6,0,1,0,10,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ExternalStatisticsResultDTO,113,0,18,0,4,0,18,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CheckoutPointBaseDTO,196,0,30,0,7,0,28,0,13,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,DeviceShopDataDTO.DeviceShopDataDTOBuilder,98,0,0,0,1,0,10,0,10,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,PingDTO,182,0,30,0,5,0,27,0,12,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopOpeningHoursDTO.ShopOpeningHoursDTOBuilder,50,0,0,0,1,0,6,0,6,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,TransactionDTO.TransactionDTOBuilder,86,0,0,0,1,0,9,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ReceiptReportCreateDTO.ReceiptReportCreateDTOBuilder,127,0,13,0,1,0,16,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,FirebaseTokenDTO,93,0,14,0,5,0,16,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CheckoutPointResponseDTO.CheckoutPointResponseDTOBuilder,37,0,0,0,1,0,4,0,4,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopPointsResponseDTO.ShopPointsResponseDTOBuilderImpl,7,0,0,0,1,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,PingDTO.PingDTOBuilder,50,0,0,0,1,0,6,0,6,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopOpeningHoursDTO,185,0,30,0,7,0,28,0,13,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ProductReportedDTO,470,0,86,0,11,0,68,0,25,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopBaseDTO,246,0,38,0,8,0,34,0,15,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,FileImageDTO.FileImageDTOBuilder,38,0,0,0,1,0,5,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,DeviceShopDataDTO,366,0,62,0,9,0,51,0,20,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopsFilterDTO,277,0,46,0,9,0,40,0,17,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ReceiptReportCreateDTO,231,0,38,0,8,0,34,0,15,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,FirebaseTokenResponseDTO,126,0,18,0,6,0,20,0,11,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopPointsResponseDTO,167,0,26,0,6,0,25,0,12,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CompanyDTO.CompanyDTOBuilder,50,0,0,0,1,0,6,0,6,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,FileImageDTO,139,0,22,0,6,0,22,0,11,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CheckoutPointRequestDTO.CheckoutPointRequestDTOBuilder,15,0,0,0,1,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ProductDTO.ProductDTOBuilder,86,0,0,0,1,0,9,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,StatisticsFilterDTO.StatisticsFilterDTOBuilder,86,0,0,0,1,0,9,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ExternalRefreshTokenRequestDTO,86,0,14,0,3,0,14,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,JwtAuthenticationCompanyResponseDTO,107,0,16,0,8,0,16,0,8,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CheckoutPointResponseDTO.CheckoutPointResponseDTOBuilderImpl,7,0,0,0,1,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,PageSortListParamDTO,248,0,38,0,8,0,37,0,18,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,NotificationCreateDTO,274,0,46,0,7,0,39,0,16,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,FirebaseDataDTO,160,0,22,0,9,0,22,0,11,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,PageSortListParamDTO.PageSortListParamDTOBuilder,92,0,6,0,1,0,10,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CheckoutPointRequestDTO,49,0,8,0,4,0,11,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,FirebaseTokenDTO.FirebaseTokenDTOBuilder,26,0,0,0,1,0,4,0,4,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,LogDTO.LogDTOBuilder,62,0,0,0,1,0,7,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopPointsDTO,139,0,22,0,6,0,22,0,11,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CheckoutPointResponseDTO,159,0,24,0,7,0,24,0,12,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopDTO.ShopDTOBuilderImpl,7,0,0,0,1,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ReceiptReportItemCreateDTO.ReceiptReportItemCreateDTOBuilder,38,0,0,0,1,0,5,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,TransactionResponseDTO,156,0,26,0,4,0,24,0,11,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopDTO.ShopDTOBuilder,81,0,0,0,2,0,8,0,8,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopDTO,346,0,52,0,11,0,46,0,20,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,TransactionItemDTO.TransactionItemDTOBuilder,74,0,0,0,1,0,8,0,8,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,LoginRequestDTO,124,0,22,0,3,0,18,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CreateNotificationDTO.CreateNotificationDTOBuilder,74,0,0,0,1,0,8,0,8,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,PagingPropertiesDTO,262,0,44,0,9,0,41,0,19,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CreateNotificationDTO,274,0,46,0,7,0,39,0,16,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CompanyDTO,185,0,30,0,7,0,28,0,13,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,OpeningHoursDTO.OpeningHoursDTOBuilder,86,0,0,0,1,0,9,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,StatisticsFilterDTO,320,0,54,0,8,0,45,0,18,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,DeviceDTO,231,0,38,0,8,0,34,0,15,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,TransactionItemDTO,274,0,46,0,7,0,39,0,16,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,PromotionProductDeviceDTO.PromotionProductDeviceDTOBuilder,38,0,0,0,1,0,5,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,StatisticsResultDTO.StatisticsResultDTOBuilder,62,0,0,0,1,0,7,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,NotificationCreateDTO.NotificationCreateDTOBuilder,74,0,0,0,1,0,8,0,8,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,DeviceConfigDTO.DeviceConfigDTOBuilder,158,0,0,0,1,0,15,0,15,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ReceiptReportItemCreateDTO,139,0,22,0,6,0,22,0,11,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CheckoutPointBaseDTO.CheckoutPointBaseDTOBuilder,43,0,0,0,1,0,5,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,PeriodDTO,139,0,22,0,6,0,22,0,11,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopPointsResponseDTO.ShopPointsResponseDTOBuilder,40,0,0,0,1,0,4,0,4,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ProductDTO,323,0,54,0,10,0,46,0,19,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,FirebaseTokenResponseDTO.FirebaseTokenResponseDTOBuilder,38,0,0,0,1,0,5,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,TransactionDTO,323,0,54,0,10,0,46,0,19,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,DeviceDTO.DeviceDTOBuilder,62,0,0,0,1,0,7,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ExternalStatisticsResultContentDTO,298,0,54,0,7,0,44,0,17,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopsFilterDTO.ShopsFilterDTOBuilder,74,0,0,0,1,0,8,0,8,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,WarningDTO,227,0,38,0,7,0,33,0,14,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,LogDTO,231,0,38,0,8,0,34,0,15,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,GeoLocationDTO,139,0,22,0,6,0,22,0,11,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,AddressDTO.AddressDTOBuilder,62,0,0,0,1,0,7,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,GeoLocationDTO.GeoLocationDTOBuilder,38,0,0,0,1,0,5,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,PromotionProductDeviceDTO,123,0,18,0,4,0,19,0,10,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopBaseDTO.ShopBaseDTOBuilder,54,0,0,0,1,0,6,0,6,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,CheckoutPointDTO,212,0,38,0,5,0,32,0,13,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,TransactionStatisticsSumDTO,227,0,38,0,7,0,33,0,14,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,ShopWithPointsDTO,323,0,54,0,10,0,46,0,19,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,PeriodDTO.PeriodDTOBuilder,38,0,0,0,1,0,5,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto,DeviceConfigDTO,573,0,94,0,16,0,78,0,31,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.util,SystemInfo,224,0,12,0,43,0,12,0,6,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.util,EncryptUtils,141,0,6,0,34,0,7,0,4,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.error,ErrorResponseDTO,107,0,14,0,9,0,16,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.error,ErrorDTO,181,0,30,0,6,0,27,0,12,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,SortDirection,64,0,4,0,11,0,7,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,Role,64,0,0,0,7,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,PromotionExpirationStatus,48,0,4,0,10,0,4,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,NotificationType,57,0,0,0,6,0,4,0,4,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,Province,219,0,0,0,22,0,6,0,6,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,ErrorCode,54,0,0,0,6,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,ConnectionType,24,0,0,0,3,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,PromotionType,59,0,0,0,6,0,3,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,ValidationErrorType,70,0,0,0,10,0,3,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,DeviceProducer,54,0,0,0,6,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,TransactionStatus,24,0,0,0,3,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,DeviceState,34,0,0,0,4,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,CashRegisterProducer,34,0,0,0,4,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,SmartConnectionType,34,0,0,0,4,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,StatisticsType,44,0,0,0,5,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,LogType,44,0,0,0,5,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,MessageStatus,34,0,0,0,4,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dict,PromotionAcceptanceStatus,34,0,0,0,4,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion.base,PromotionItemBaseDTO.PromotionItemBaseDTOBuilder,32,0,0,0,1,0,4,0,4,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion.base,PromotionBaseDTO.PromotionBaseDTOBuilder,120,0,0,0,1,0,12,0,12,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion.base,PromotionBaseDTO,569,0,82,0,15,0,69,0,28,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.dto.promotion.base,PromotionItemBaseDTO,145,0,22,0,6,0,21,0,10,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.model,LogEntity,67,0,0,0,21,0,14,0,14,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.model,SdCardReport,3,0,0,0,1,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.model,ElzabProduct,59,0,0,0,25,0,17,0,17,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.model,Product,3,0,0,0,1,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.model,Receipt,57,0,0,0,17,0,12,0,12,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.model,NameEanCache,3,0,0,0,1,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.model,Promo,3,0,0,0,1,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.model,ReceiptItem,78,0,0,0,20,0,14,0,14,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.model,Promo.DeviceStatus,44,0,0,0,1,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.exceptions,NoPromotionException,13,0,0,0,2,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.fake,FakeRegisterModule,668,0,46,0,128,0,43,0,20,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.fake,FakeProductDTO,33,0,0,0,14,0,7,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,AddPromoResponseItemDTO,307,0,50,0,8,0,43,0,18,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,TransactionItemDTO,45,0,0,0,19,0,13,0,13,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,PromoDTO,17,0,0,0,7,0,5,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,PromoItemDiscountDTO,31,0,0,0,13,0,9,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,AddPromoResponseDTO,136,0,22,0,4,0,21,0,10,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,PromoItemDTO,38,0,0,0,16,0,11,0,11,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,PromoItemDiscountDTO.PCT_VALUE_QTY,44,0,0,0,1,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,TransactionDTO,45,0,0,0,19,0,13,0,13,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,AddPromoResponseItemDTO.AddPromoResponseItemDTOBuilder,86,0,0,0,1,0,9,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,PromoItemDTO.PCT_VALUE_QTY,44,0,0,0,1,0,1,0,1,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,ShopInfoDTO,24,0,0,0,10,0,7,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,DeviceInfoDTO,52,0,0,0,11,0,8,0,8,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.dto,AddPromoResponseDTO.AddPromoResponseDTOBuilder,103,0,13,0,1,0,14,0,7,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,SendLogsTask,93,0,10,0,19,0,7,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,JobMonitorTask,96,0,8,0,22,0,7,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,ReadConfigTask,116,0,14,0,35,0,10,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,PromoTask,268,0,22,0,55,0,14,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,ReadPromoTask,28,0,4,0,9,0,4,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,LoginTask,141,0,10,0,32,0,8,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,SbAbstractTask,100,0,4,0,29,0,6,0,4,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,AddFakePromoTask,132,0,4,0,34,0,4,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,UpdateCheckoutPointTask,96,0,6,0,27,0,5,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,SendConfigTask,34,0,2,0,7,0,3,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,SendTransactionsTask,174,0,14,0,41,0,9,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,RefreshTokenTask,22,0,0,0,6,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,PingTask,29,0,0,0,11,0,2,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,SendSdReportTask,542,0,66,0,114,0,38,0,5,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.task,WebTask,351,0,42,0,68,0,27,0,6,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.service,LogService,103,0,10,0,21,0,8,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.service,TransactionService,335,0,20,0,73,0,18,0,8,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.service,NovitusService,679,0,24,0,109,0,21,0,9,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.contoller,TaskController,148,0,4,0,49,0,21,0,19,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.contoller,SpecialControler,16,0,2,0,6,0,3,0,2,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.contoller,MonitoringController,47,0,4,0,10,0,5,0,3,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.novitus,NovitusModule,1999,198,183,25,429,54,119,9,20,4\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.novitus,NovitusProductDTO,77,17,0,0,33,7,22,5,22,5\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.novitus,TransmissionState,0,124,0,0,0,13,0,1,0,1\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.novitus,NovitusCardReader.CmdResponse,2,67,2,6,1,16,2,3,0,1\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.novitus,NovitusTransmission,408,124,75,8,100,31,63,8,14,7\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.novitus,NovitusErrorPacket,164,0,12,0,39,0,20,0,14,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.novitus,NovitusDataPacket,185,0,10,0,40,0,16,0,11,0\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.novitus,NovitusStartPacket,144,22,6,0,32,8,16,2,13,2\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.novitus,TransmissionType,24,49,4,0,4,7,3,2,1,2\r\n" + 
			"smartbox-pi,pl.strefahandlu.app.register.novitus,NovitusCardReader,912,0,38,0,151,0,34,0,15,0\r\n" + 
			"";

	private JacocoParser parser;
	private TestResult result;
	private List<Metric> allMetrics;
	private Metric metric;
	
	@BeforeEach
	public void before() {
		parser = new JacocoParser();
		Project project = new Project();
		project.setSizeOfProject(12);
		project.setHash("project_hash");
		project.setDevelopers(2);
		
		result = new TestResult();
		result.setResult(REPORT);
		result.setCreated(new Date());
		result.setProject(project);
		result.setType(TestResultType.JUNIT_TESTS);
		
		allMetrics = new ArrayList<Metric>();
		
		
		metric = new Metric();
		metric.setProject(project);
	}
	
	@Test
	void parseCoverage_correctlyFilledCoverage() {
		// given
		int missing= 6109;
		int covered= 145;
		BigDecimal allLines = new BigDecimal(missing + covered);
		BigDecimal coveredLines = new BigDecimal(covered);
		BigDecimal coverage = coveredLines.divide(allLines, MathContext.DECIMAL32)
				.multiply(new BigDecimal(100))
				.setScale(2, RoundingMode.HALF_UP);

		// when
		parser.updateMetric(result, metric, allMetrics);

		// then
		assertThat(metric.getCoverage()).isEqualTo(coverage);
	}

}
