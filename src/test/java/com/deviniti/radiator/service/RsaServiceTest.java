package com.deviniti.radiator.service;

import java.io.IOException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.junit.jupiter.api.Test;

public class RsaServiceTest {

	
	private RsaService rsaService = new RsaService();
	
	
	
	@Test
	public void generateRsaTest() throws NoSuchAlgorithmException, IOException
	{
		KeyPair p = rsaService.generateRsaKeys();
		System.out.println(p.getPublic());
		System.out.println(p.getPrivate());

		System.out.println(Base64.getMimeEncoder().encodeToString(p.getPrivate().getEncoded()));
		System.out.println("--");
		System.out.println(Base64.getEncoder().encodeToString(p.getPrivate().getEncoded()));

	}
}
