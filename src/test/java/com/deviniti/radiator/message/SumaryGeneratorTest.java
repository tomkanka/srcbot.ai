package com.deviniti.radiator.message;


import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.deviniti.radiator.messages.SummaryGenerator;
import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.model.Summary;
import com.deviniti.radiator.repository.NoteRepository;
import com.deviniti.radiator.repository.SummaryRepository;

@ExtendWith(MockitoExtension.class)
public class SumaryGeneratorTest {

	
	@Mock
	NoteRepository noteRepository;
	
	@Mock
	SummaryRepository summaryRepository;
	
	@InjectMocks
	SummaryGenerator summaryGenerator = new SummaryGenerator();
	
	@Test
	public void testJoke() {
		
//		String NO_JOKE_MESSAGE = "no joke message";
//		String JOKE_MESSAGE = "no joke message";
		String PROJECT_NAME = "PROJECT_NAME kjdsfladsj fsdhkdsjhgkjdsgkfdhgkhd";

		
//		Note nojoke = new Note();
//		nojoke.setLogicalCondition("avg_new_lines_7d>=1");
//		nojoke.setContent(NO_JOKE_MESSAGE);
//		
//		Note joke = new Note();
//		joke.setContent(JOKE_MESSAGE);

		Project project = new Project();
		project.setName(PROJECT_NAME);
		
		Metric metric = new Metric();
		metric.setAvgNewLines7d(BigDecimal.ZERO);

		
//		Mockito.when(noteRepository.findRandomNoteNotJoke()).thenReturn(Arrays.asList(nojoke));
//		Mockito.when(noteRepository.findRandomNoteByType("JOKE")).thenReturn(joke);

		Summary summary = new Summary();
		summary.setParagraph("paragrph <<name>>");
		
		Mockito.when(summaryRepository.findRandomPartSummary(Mockito.anyString())).thenReturn(summary );
		
		String message = summaryGenerator.getMessage(project , metric );
		System.out.println(message);

		assertThat(message).contains(PROJECT_NAME);

	}

	
	

}
