package com.deviniti.radiator.message;


import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.deviniti.radiator.messages.NoteGenerator;
import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Note;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.repository.NoteRepository;

@ExtendWith(MockitoExtension.class)
public class NoteGeneratorTest {

	
	@Mock
	NoteRepository noteRepository;
	
	@InjectMocks
	NoteGenerator noteGenerator = new NoteGenerator();
	
	@Test
	public void testJoke() {
		
		String NO_JOKE_MESSAGE = "no joke message";
		String JOKE_MESSAGE = "joke message <<joke_icons>>";
		
		String PROJECT_NAME = "kjdsfladsj fsdhkdsjhgkjdsgkfdhgkhd";
		
		Note nojoke = new Note();
		nojoke.setLogicalCondition("avg_new_lines_7d>=1");
		nojoke.setContent(NO_JOKE_MESSAGE);
		
		Note joke = new Note();
		joke.setContent(JOKE_MESSAGE);
		
		Mockito.when(noteRepository.findRandomNoteNotJoke()).thenReturn(Arrays.asList(nojoke));
		
		Mockito.when(noteRepository.findRandomNoteByType("JOKE")).thenReturn(joke);
		
		Project project = new Project();
		project.setName(PROJECT_NAME);
		Metric metric = new Metric();

		metric.setAvgNewLines7d(BigDecimal.ZERO);
		String message = noteGenerator.getMessage(project , metric );
		System.out.println(message);
		assertThat(message).startsWith(JOKE_MESSAGE.replaceAll("<<joke_icons>>", ""));
		assertThat(message.contains("<<joke_icons>>")).isFalse();

		metric.setAvgNewLines7d(BigDecimal.ONE);
		String message2 = noteGenerator.getMessage(project , metric );
		System.out.println(message2);
		assertThat(message2).isEqualTo(NO_JOKE_MESSAGE);

	}

	
	

}
