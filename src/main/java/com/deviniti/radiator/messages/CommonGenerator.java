package com.deviniti.radiator.messages;

import java.util.Random;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;

public abstract class CommonGenerator {

	static String[] greetingEmos = new String[] {":bowtie:", ":blush:", ":sunny:",":clock930:",":rocket:",":dizzy:",":loudspeaker:",":mega:"};
	static String[] smileEmos = new String[] {":smile:",":blush:",":smiley:"};
	static String[] sadEmos = new String[] {":anguished:",":scream:",":disappointed:"};
	static String[] jokeEmos = new String[] {":stuck_out_tongue_winking_eye:",":sweat_smile:",":smiley:",":joy:",":boom:",":joy_cat:",":smile_cat:",":black_joker:"};
	
	
	
	public abstract String getMessage(Project project, Metric metric);
	
	

	protected String replaceTags(String message, Project project, Metric metric) {
	

		message = message.replaceAll("<<greeting_icons>>", randomEmo(greetingEmos, 3));
		message = message.replaceAll("<<end_icons>>", randomEmo(greetingEmos, 1));
		message = message.replaceAll("<<joke_icons>>", randomEmo(jokeEmos,1));

		message = message.replaceAll("<<name>>", project.getName());
		message = message.replaceAll("<<days_to_finish>>",String.format("%,d", metric.getDaysToFinish()));
		message = message.replaceAll("<<last_new_lines>>",String.format("%,d", metric.getLastNewLines()));
		message = message.replaceAll("<<last_modified_lines>>",String.format("%,d", metric.getLastModifiedLines()));
		message = message.replaceAll("<<last_deleted_lines>>",String.format("%,d", metric.getLastDeletedLines()));
		message = message.replaceAll("<<last_new_lines_per_developer>>",String.format("%,d", metric.getLastNewLinesPerDeveloper()));

		
		String smile = randomEmo(smileEmos,1);
		String sad = randomEmo(sadEmos,1);

		if ((metric.getDiffFromPreviousWeekNewLines()!=null) && (metric.getDiffFromPreviousWeekNewLines().floatValue()!=0)) {
			int p = (int) Math.round(100.0 * metric.getDiffFromPreviousWeekNewLines().floatValue());
			if (p>0) {
				message = message.replaceAll("<<diff_from_previous_week_new_lines>>",String.format("%,d", p)+"% more");
				message = message.replaceAll("<<diff_from_previous_week_new_lines_emo>>",smile);
			} else {
				message = message.replaceAll("<<diff_from_previous_week_new_lines>>",String.format("%,d", (-1*p))+"% less");
				message = message.replaceAll("<<diff_from_previous_week_new_lines_emo>>",sad);
			}
		}
		
		if ((metric.getDiffOtherTeamsNewLines()!=null) && (metric.getDiffOtherTeamsNewLines().floatValue()!=0)) {
			int p = (int) Math.round(100.0 * metric.getDiffOtherTeamsNewLines().floatValue());
			if (p>0) {
				message = message.replaceAll("<<diff_other_teams_new_lines>>",String.format("%,d", p)+"% more");
				message = message.replaceAll("<<diff_other_teams_new_lines_emo>>",smile);
			} else {
				message = message.replaceAll("<<diff_other_teams_new_lines>>",String.format("%,d", (-1*p))+"% less");
				message = message.replaceAll("<<diff_other_teams_new_lines_emo>>",sad);
			}			
		}
		
		if ((metric.getUnitTests()!=null) && (metric.getUnitTests()>0)) {
			message = message.replaceAll("<<total_unit_tests>>",String.format("%,d", metric.getUnitTests()));
			message = message.replaceAll("<<last_new_unit_tests>>",String.format("%,d", metric.getLastUnitTests()));
			message = message.replaceAll("<<total_number_of_lines>>",String.format("%,d", metric.getTotalLines()));
			
			if (metric.getDaysFromLastCheck() != null)
			{
				if (metric.getDaysFromLastCheck()==1) {
					message = message.replaceAll("<<days_from_last_check>>","1st day");
				} else {
					message = message.replaceAll("<<days_from_last_check>>",metric.getDaysFromLastCheck().toString()+" days");
				}
			}
			else
				message = message.replaceAll("<<days_from_last_check>>", "???");
			
		}
		
		if ((metric.getTotalStaticErrorsChange()!=null) && (metric.getTotalStaticErrorsChange()>0)) {
			message = message.replaceAll("<<total_static_errors>>",String.format("%,d", metric.getTotalStaticErrorsChange()));
		}

		
		return message;
	}

	
	private String randomEmo(String[] emo, int count) {
		
		if (count<=0) 
			count=1;
		
        Random r = new Random();
        int index = r.nextInt(emo.length);		
		
		StringBuffer result = new StringBuffer();
		
		for (int i = 0; i < count; i++) {
			result.append(emo[index]);
		}
		
		return result.toString();
	}
	


}
