package com.deviniti.radiator.messages;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Note;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.repository.NoteRepository;

@Component
public class NoteGenerator extends CommonGenerator {
	
	@Autowired
	NoteRepository noteRepository;
	
	private Random random = new Random();

	  public String getMessage(Project project, Metric metric) {
				
		ScriptEngineManager factory = new ScriptEngineManager();
		ScriptEngine engine = factory.getEngineByName("JavaScript");		
			 
		
		String message = null;

		try {
			engine.eval("avg_new_lines_7d="+(metric.getAvgNewLines7d()==null?"0":metric.getAvgNewLines7d().toString())+";");
			engine.eval("avg_new_unit_tests_7d="+(metric.getAvgNewUnitTests7d()==null?"0":metric.getAvgNewUnitTests7d().toString())+";");
			engine.eval("avg_static_errors_7d="+(metric.getAvgStaticErrors7d()==null?"0":metric.getAvgStaticErrors7d().toString())+";");
			
			List<Note> notes = new ArrayList<Note>();
			for (Note note : noteRepository.findRandomNoteNotJoke()) {
				String condition = note.getLogicalCondition();
				System.out.println(condition + " => " + engine.eval(condition));
				if (engine.eval(condition).toString().equals("true")) {

					notes.add(note);
				}
			}
			if (! notes.isEmpty())
			{
			      int index = random.nextInt(notes.size());
			      message = notes.get(index).getContent();
			}
			
		} catch (ScriptException e1) {
			//
		}

		
		if (message == null)
		{
			Note noteJoke = noteRepository.findRandomNoteByType("JOKE");		
			message = noteJoke.getContent();
		}
		
		return replaceTags(message, project, metric);
	}
	
	

}
