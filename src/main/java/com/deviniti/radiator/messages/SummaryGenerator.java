package com.deviniti.radiator.messages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.repository.SummaryRepository;

@Component
public class SummaryGenerator extends CommonGenerator {
	
	@Autowired
	SummaryRepository summaryRepository;
		

	public String getMessage(Project project, Metric metric) {
		
		
		String message = summaryRepository.findRandomPartSummary("GREETING").getParagraph()+"\n";
		message += summaryRepository.findRandomPartSummary("BASIC_METRICS").getParagraph()+"\n";
		
		if ((metric.getDiffFromPreviousWeekNewLines()!=null) && (metric.getDiffFromPreviousWeekNewLines().floatValue()!=0)) {
			message += summaryRepository.findRandomPartSummary("BASIC_METRICS_LAST_WEEK").getParagraph()+"\n";
		}

		if ((metric.getDiffOtherTeamsNewLines()!=null) && (metric.getDiffOtherTeamsNewLines().floatValue()!=0)) {
			message += summaryRepository.findRandomPartSummary("BASIC_METRICS_OTHER_TEAMS").getParagraph()+"\n";
		}	
		
		if ((metric.getUnitTests()!=null) && (metric.getUnitTests()>0)) {
			message += summaryRepository.findRandomPartSummary("UNIT_TEST_METRICS").getParagraph()+"\n";
		}

		if ((metric.getTotalStaticErrorsChange()!=null) && (metric.getTotalStaticErrorsChange()>0)) {
			message += summaryRepository.findRandomPartSummary("STATIC_TEST_METRICS").getParagraph()+"\n";
		}		
		
		message += summaryRepository.findRandomPartSummary("ENDING").getParagraph();
		
		
		return replaceTags(message, project, metric);
	}
	
	protected String replaceTags(String message, Project project, Metric metric) {

		message = super.replaceTags(message, project, metric);
		
		message = message.replaceAll("<<last_check>>", "Yesterday");

		return message;
	}

}
