package com.deviniti.radiator.metrics;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deviniti.radiator.dto.SlackRequest;
import com.deviniti.radiator.messages.NoteGenerator;
import com.deviniti.radiator.messages.SummaryGenerator;
import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.repository.MetricsRepository;

@Component
public class MetricManager {

	@Autowired
	MetricsRepository metricsRepository;
	
	@Autowired
	SummaryGenerator summaryGenerator;
	
	@Autowired
	NoteGenerator noteGenerator;
		
	public SlackRequest prepareSummaryMessage(Project project) {

		List<Metric> projectMetrics = metricsRepository.findByProjectAndSendedOrderByCreatedDesc(project, false);
		if ((projectMetrics != null) && (!projectMetrics.isEmpty())) {
			Metric metric = projectMetrics.get(0);
			String message = summaryGenerator.getMessage(project,metric);
			SlackRequest request = new SlackRequest(message);
			return request;

		} 
		return null;
	}

	public SlackRequest prepareNotificationMessage(Project project) {
		
		List<Metric> projectMetrics = metricsRepository.findByProjectAndSendedOrderByCreatedDesc(project, false);
		if ((projectMetrics != null) && (!projectMetrics.isEmpty())) {
			Metric metric = projectMetrics.get(0);
			String message = noteGenerator.getMessage(project,metric);
			SlackRequest request = new SlackRequest(message);
			return request;
		};
		
		return null;
		
	}

		
	
}
