package com.deviniti.radiator.metrics;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.model.TestResult;

public class ClocParser implements Parser {
	
	private static final String WHITE_CHAR_MARK = "+";
	private ParserUtil util = new ParserUtil();

	public void updateMetric(TestResult report, Metric metric, List<Metric> allMetrics) {
		if(!validate(report)) {
			return;
		}
		updateTotalLines(metric, report);
		updateDaysToFinish(metric, report.getProject());
	}

	private void updateDaysToFinish(Metric metric, Project project) {
		int daysToFinish = calculateDaysOfProject(project);
		metric.setDaysToFinish(daysToFinish);
	}

	private int calculateDaysOfProject(Project project) {
		return Optional.ofNullable(project)
				.map(Project::getCreated)
				.map(created -> {
					GregorianCalendar calendar = new GregorianCalendar();
					calendar.setTime(created);
					return calendar;
				})
				.map(calendar -> {
					calendar.add(GregorianCalendar.DAY_OF_MONTH, 
							Optional.ofNullable(project)
									.map(Project::getSizeOfProject)
									.orElse(0));
					return calendar;
				})
				.map(finishedDay -> finishedDay.getTime())
				.map(finishedDate -> calculateLastDays(new Date(), finishedDate))
				.orElse(0);
	}

	private int calculateLastDays(Date beforeDate, Date afterDate) {
		GregorianCalendar actual = new GregorianCalendar();
		actual.setTime(afterDate);
		GregorianCalendar last = new GregorianCalendar();
		last.setTime(beforeDate);
		int days=0;
		if(actual.get(GregorianCalendar.YEAR)-last.get(GregorianCalendar.YEAR)>100) {
			return 100;
		}else if(actual.get(GregorianCalendar.YEAR)-last.get(GregorianCalendar.YEAR)>2) {
			while(last.before(actual)) {
				days+=365;
				last.add(GregorianCalendar.YEAR, 1);
			}
		}else {
			while(last.before(actual)) {
				int dayOfWeek = last.get(GregorianCalendar.DAY_OF_WEEK);
				if(dayOfWeek!=1 && dayOfWeek!=7) {
					days++;
				}
				last.add(GregorianCalendar.DAY_OF_MONTH, 1);
			}
		}
		return days;
	}

	private void updateTotalLines(Metric metric, TestResult testResult) {
		int totalLines = getTotalLines(testResult);
		metric.setTotalLines(totalLines);
	}

	private int getTotalLines(TestResult testResult) {
		return Optional.ofNullable(testResult)
				.map(TestResult::getResult)
				.map(input -> input.split("\n"))
				.map(Arrays::asList)
				.orElseGet(() -> Collections.emptyList())
				.stream()
				.filter(token -> token.contains("SUM:"))
				.findFirst()
				.map(token -> token.trim())
				.map(line -> line.replaceAll("\\s+", WHITE_CHAR_MARK))
				.map(replaced -> replaced.substring(replaced.lastIndexOf(WHITE_CHAR_MARK)+1))
				.map(totalAsString -> new BigDecimal(totalAsString).intValue())
				.orElse(-1);
	}
	
}
