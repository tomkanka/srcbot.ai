package com.deviniti.radiator.metrics;

import java.util.List;
import java.util.Optional;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.TestResult;

public interface Parser {

	public void updateMetric(TestResult report, Metric metric, List<Metric> allMetrics);
	
	
	public default boolean validate(TestResult report) {
		return Optional.ofNullable(report)
				.map(TestResult::getResult)
				.filter(text -> !text.isBlank())
				.isPresent();
	}
}
