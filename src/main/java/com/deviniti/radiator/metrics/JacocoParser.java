package com.deviniti.radiator.metrics;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.TestResult;

public class JacocoParser implements Parser {
	
	private static final int MISSED_LINES_INDEX = 7;
	private static final int COVERED_LINES_INDEX = 8;

	public void updateMetric(TestResult report, Metric metric, List<Metric> allMetrics) {
		if(!validate(report)) {
			return;
		}
		metric.setCoverage(getCoverage(report));
	}

	private BigDecimal getCoverage(TestResult report) {
		ReportDTO dto = sumUpLines(report);
		return new BigDecimal(dto.coveragedLines)
				.divide(new BigDecimal(dto.getAllLines()), MathContext.DECIMAL32)
				.multiply(new BigDecimal(100))
				.setScale(2, RoundingMode.HALF_UP);
	}

	private ReportDTO sumUpLines(TestResult report) {
		return Optional.ofNullable(report)
				.map(TestResult::getResult)
				.map(result -> result.split("\n"))
				.map(Arrays::asList)
				.orElseGet(() -> Collections.emptyList())
				.stream()
				.map(String::trim)
				.filter(line -> !line.contains("LINE_MISSED"))
				.map(this::mapToReportDTO)
				.reduce(new ReportDTO(), ReportDTO::add);
	}
	
	private ReportDTO mapToReportDTO(String line) {
		ReportDTO dto = new ReportDTO();
		String[] cells = line.split(",");
		dto.missingLines = new BigDecimal(cells[MISSED_LINES_INDEX]).intValue();
		dto.coveragedLines = new BigDecimal(cells[COVERED_LINES_INDEX]).intValue();
		return dto;
	}

	class ReportDTO {
		private int coveragedLines;
		private int missingLines;
		
		public ReportDTO add(ReportDTO that) {
			this.coveragedLines += that.coveragedLines;
			this.missingLines += that.missingLines;
			return this;
		}
		
		public int getAllLines() {
			return coveragedLines + missingLines;
		}
	}
}
