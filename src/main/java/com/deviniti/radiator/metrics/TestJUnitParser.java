package com.deviniti.radiator.metrics;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.model.TestResult;

public class TestJUnitParser implements Parser {
	
	private static final String DELIM_TAG = ",";
	private static final String TEST_TAG = "Tests run:";
	private static final String FAIL_TAG = "Failures:";
	private static final String ERROR_TAG = "Errors:";
	private ParserUtil util = new ParserUtil();

	public void updateMetric(TestResult report, Metric metric, List<Metric> allMetrics) {
		if(!validate(report)) {
			return;
		}
		TestReportDTO testReport = getTestReport(report);
		metric.setUnitTests(testReport.tests);
		metric.setHeighErrors(testReport.failures + testReport.errors);
		updateLastTestsAndHeighErrors(report, metric, allMetrics);
		updateAverageLastTestsAndHeighErrors(report, metric, allMetrics);
		updateTotalAverageLastTestAndHeighErrors(report, metric, allMetrics);
		updateLastNewUnitTest(metric, allMetrics);
		updateTotalStaticErrorsChange(metric, allMetrics);
		updateLastStaticErrorsChange(metric, allMetrics);
		updateAvgNewUnitTests7d(metric, allMetrics);
		updateAvgStaticErrors7d(metric, allMetrics);
	}
	

	private void updateAvgStaticErrors7d(Metric metric, List<Metric> allMetrics) {
		Integer newTestsInLastWeek = filterMetricsFormLastWeek(metric, allMetrics)
				.stream()
				.map(temp -> temp.getHeighErrors())
				.reduce(0, (sum, item) -> sum+item);
		BigDecimal average = new BigDecimal(newTestsInLastWeek)
				.divide(new BigDecimal(5), MathContext.DECIMAL32)
				.setScale(2, RoundingMode.HALF_UP);
		metric.setAvgStaticErrors7d(average);
	}


	private void updateAvgNewUnitTests7d(Metric metric, List<Metric> allMetrics) {
		Integer newTestsInLastWeek = filterMetricsFormLastWeek(metric, allMetrics)
				.stream()
				.map(temp -> temp.getLastUnitTests())
				.reduce(0, (sum, item) -> sum+item);
		BigDecimal average = new BigDecimal(newTestsInLastWeek)
				.divide(new BigDecimal(5), MathContext.DECIMAL32)
				.setScale(2, RoundingMode.HALF_UP);
		metric.setAvgNewUnitTests7d(average);
	}


	private List<Metric> filterMetricsFormLastWeek(Metric metric, List<Metric> allMetrics) {
		List<Metric> projectMetrics = filterProjectMetrics(metric, allMetrics);
		Date cutoff = getCutOfDate(metric);
		return projectMetrics.stream()
				.filter(temp -> temp.getCreated().after(cutoff))
				.collect(Collectors.toList());
	}
	
	private List<Metric> filterProjectMetrics(Metric metric, List<Metric> allMetrics) {
		List<Metric> list = allMetrics.stream()
				.filter(temp -> isBelongToProject(temp, metric.getProject()))
				.collect(Collectors.toList());
		list.add(metric);
		return list;
	}
	
	private boolean isBelongToProject(Metric metric, Project project) {
		return Optional.ofNullable(metric)
				.map(Metric::getProject)
				.map(Project::getHash)
				.map(hash -> hash.equals(project.getHash()))
				.orElse(false);
	}

	private Date getCutOfDate(Metric metric) {
		GregorianCalendar cutoff = new GregorianCalendar();
		cutoff.setTime(metric.getCreated());
		cutoff.add(GregorianCalendar.DAY_OF_MONTH, -7);
		return cutoff.getTime();
	}

	
	private void updateLastStaticErrorsChange(Metric metric, List<Metric> allMetrics) {
		Metric lastMetric = getLastMetric(allMetrics, metric.getProject());
		int errorsChange = Optional.ofNullable(lastMetric)
				.map(Metric::getTotalStaticErrorsChange)
				.map(lastHeighErrors -> Optional.ofNullable(metric)
						.map(Metric::getTotalStaticErrorsChange)
						.orElse(0) - lastHeighErrors)
				.orElse(0);
		metric.setLastStaticErrorsChange(errorsChange);
	}

	private void updateTotalStaticErrorsChange(Metric metric, List<Metric> allMetrics) {
		Metric lastMetric = getLastMetric(allMetrics, metric.getProject());
		int errorsChange = Optional.ofNullable(lastMetric)
				.map(Metric::getHeighErrors)
				.map(lastHeighErrors -> Optional.ofNullable(metric)
						.map(Metric::getHeighErrors)
						.orElse(0) - lastHeighErrors)
				.orElse(0);
		metric.setTotalStaticErrorsChange(errorsChange);
	}


	private void updateLastNewUnitTest(Metric metric, List<Metric> allMetrics) {
		Metric lastMetric = getLastMetric(allMetrics, metric.getProject());
		int lastUnitTests = Optional.ofNullable(lastMetric)
				.map(Metric::getUnitTests)
				.map(lastTestsNumber -> Optional.ofNullable(metric)
						.map(Metric::getUnitTests)
						.orElse(0) - lastTestsNumber)
				.orElse(0);
		metric.setLastUnitTests(lastUnitTests);
	}

	private Metric getLastMetric(List<Metric> allMetrics, Project project) {
		Metric last = null;
		for(Metric metric: allMetrics) {
			if(metric.getProject().equals(project)) {
				if(last == null) {
					last = metric;
				}else if(last.getCreated().before(metric.getCreated())) {
					last = metric;
				}
			}
		}
		return last;
	}


	private void updateTotalAverageLastTestAndHeighErrors(TestResult report, Metric metric, List<Metric> allMetrics) {
		List<Metric> metrics = new ArrayList<Metric>(allMetrics);
		metrics.add(metric);
		metric.setTotalAverageNewUnitTests(calculateTotalAgerageNewTests(metrics));
		metric.setTotalAverageHeighErrorsChange(calculateTotalHeighErrorsChange(metrics));
	}

	private BigDecimal calculateTotalHeighErrorsChange(List<Metric> metrics) {
		if(metrics.isEmpty()) {
			return BigDecimal.ZERO;
		}
		return getSumOfNewHeighErrors(metrics)
					.divide(new BigDecimal(metrics.size()), MathContext.DECIMAL32)
					.divide(getDevelopers(metrics), MathContext.DECIMAL32)
					.setScale(2, RoundingMode.HALF_UP);
	}

	private BigDecimal calculateTotalAgerageNewTests(List<Metric> metrics) {
		if(metrics.isEmpty()) {
			return BigDecimal.ZERO;
		}
		return getSumOfNewTests(metrics)
					.divide(new BigDecimal(metrics.size()), MathContext.DECIMAL32)
					.divide(getDevelopers(metrics), MathContext.DECIMAL32)
					.setScale(2, RoundingMode.HALF_UP);
	}

	private BigDecimal getDevelopers(List<Metric> metrics) {
		Set<Project> collect = metrics.stream()
					.map(Metric::getProject)
					.collect(Collectors.toSet());
		return new BigDecimal(collect
					.stream()
					.map(Project::getDevelopers)
					.map(developers -> developers != null? (int) developers: 0)
					.reduce(0, (sum, item) -> sum + item)
					);
	}

	private void updateAverageLastTestsAndHeighErrors(TestResult report, Metric metric, List<Metric> allMetrics) {
		List<Metric> projectMetrics = util.getAllMetricsOfProject(allMetrics, report.getProject());
		projectMetrics.add(metric);
		metric.setAverageNewUnitTests(calculateAverageNewTests(projectMetrics, report.getProject()));
		metric.setAverageHeighErrorsChange(calculateAverageHeighErrorsChange(projectMetrics, report.getProject()));
	}

	private BigDecimal calculateAverageHeighErrorsChange(List<Metric> projectMetrics, Project project) {
		return getDevelopers(project).equals(BigDecimal.ZERO) || projectMetrics.isEmpty()? 
				BigDecimal.ZERO:
				getSumOfNewHeighErrors(projectMetrics)
					.divide(new BigDecimal(projectMetrics.size()), MathContext.DECIMAL32)
					.divide(getDevelopers(project), MathContext.DECIMAL32)
					.setScale(2, RoundingMode.HALF_UP);
	}

	private BigDecimal getSumOfNewHeighErrors(List<Metric> projectMetrics) {
		return new BigDecimal(projectMetrics.stream()
				.map(Metric::getLastHeighErrorsChange)
				.filter(lastHeighErrors -> lastHeighErrors!=null)
				.reduce(0, Integer::sum)
				);
	}

	private BigDecimal calculateAverageNewTests(List<Metric> projectMetrics, Project project) {
		return getDevelopers(project).equals(BigDecimal.ZERO) || projectMetrics.isEmpty()? 
				BigDecimal.ZERO:
				getSumOfNewTests(projectMetrics)
					.divide(new BigDecimal(projectMetrics.size()), MathContext.DECIMAL32)
					.divide(getDevelopers(project), MathContext.DECIMAL32)
					.setScale(2, RoundingMode.HALF_UP);
	}

	private BigDecimal getSumOfNewTests(List<Metric> projectMetrics) {
		return new BigDecimal(projectMetrics.stream()
				.map(Metric::getLastUnitTests)
				.filter(lastUnitTests -> lastUnitTests!=null)
				.reduce(0, (sum, item) -> sum+item)
				);
	}

	private BigDecimal getDevelopers(Project project) {
		return Optional.ofNullable(project)
				.map(Project::getDevelopers)
				.map(BigDecimal::new)
				.orElse(BigDecimal.ZERO);
	}
	
	private void updateLastTestsAndHeighErrors(TestResult report, Metric metric, List<Metric> allMetrics) {
		Metric lastMetric = util.getLastMetric(allMetrics, report.getProject());
		updateLastNewTests(metric, lastMetric);
		updateLastHeihErrors(metric, lastMetric);
	}

	private void updateLastHeihErrors(Metric metric, Metric lastMetric) {
		int actual = metric.getHeighErrors();
		int last = lastMetric!= null? lastMetric.getHeighErrors(): 0;
		metric.setLastHeighErrorsChange(actual - last);
	}

	private void updateLastNewTests(Metric metric, Metric lastMetric) {
		int actual = metric.getUnitTests();
		int last = lastMetric!= null? lastMetric.getUnitTests(): 0;
		metric.setLastUnitTests(actual - last);
	}

	@SuppressWarnings("unchecked")
	private TestReportDTO getTestReport(TestResult report) {
		return Optional.ofNullable(report)
				.map(TestResult::getResult)
				.map(input -> input.split("\n"))
				.map(Arrays::asList)
				.orElseGet(() -> Collections.emptyList())
				.stream()
				.filter(line -> !line.contains("Time elapsed:"))
				.findAny()
				.map(line -> line.trim())
				.map(this::mapToDto)
				.orElseGet(() -> new TestReportDTO());
	}

	private TestReportDTO mapToDto(String line) {
		TestReportDTO dto = new TestReportDTO();
		dto.tests=getValue(TEST_TAG, line);
		dto.failures=getValue(FAIL_TAG, line);
		dto.errors=getValue(ERROR_TAG, line);
		return dto;
	}

	public int getValue(String tag, String line) {
		return Optional.ofNullable(line)
				.map(ln -> ln.substring(ln.indexOf(tag)+tag.length()))
				.map(temp -> temp.substring(0, temp.indexOf(DELIM_TAG)))
				.map(tmp -> tmp.trim())
				.map(BigDecimal::new)
				.map(BigDecimal::intValue)
				.orElse(0);
	}

	class TestReportDTO{
		private int tests;
		private int failures;
		private int errors;
		
		public TestReportDTO add(TestReportDTO that) {
			this.tests += that.tests;
			this.failures += that.failures;
			this.errors += that.errors;
			return this;
		}
		
	}
}
