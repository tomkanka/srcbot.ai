package com.deviniti.radiator.metrics;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.model.TestResult;

public class GeneralParser implements Parser{
	
	private ParserUtil util = new ParserUtil();

	@Override
	public void updateMetric(TestResult report, Metric metric, List<Metric> allMetrics) {
		if(!validate(report)) {
			return;
		}
		updateCreated(metric, report);	
		updateLastDays(metric, allMetrics, report.getProject());
	}
	
	private void updateLastDays(Metric metric, List<Metric> allMetrics, Project project) {
		Metric lastMetric = util.getLastMetric(allMetrics, project);
		int lastDays = lastMetric!=null? calculateLastDays(lastMetric.getCreated(), metric.getCreated()): 0;
		metric.setLastDays(lastDays);
		metric.setDaysFromLastCheck(lastDays);
	}
	
	private int calculateLastDays(Date beforeDate, Date afterDate) {
		GregorianCalendar actual = new GregorianCalendar();
		actual.setTime(afterDate);
		GregorianCalendar last = new GregorianCalendar();
		last.setTime(beforeDate);
		int days=0;
		while(last.before(actual)) {
			int dayOfWeek = last.get(GregorianCalendar.DAY_OF_WEEK);
			if(dayOfWeek!=1 && dayOfWeek!=7) {
				days++;
			}
			last.add(GregorianCalendar.DAY_OF_MONTH, 1);
		}
		return days;
	}
	
	private void updateCreated(Metric metric, TestResult testResult) {
		metric.setCreated(new Date());
	}
}
