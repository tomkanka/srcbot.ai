package com.deviniti.radiator.metrics;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.model.TestResult;

public class GitLinesParser implements Parser {
	
	private static final String WHITE_CHAR_MARK="+";

	public void updateMetric(TestResult report, Metric metric, List<Metric> allMetrics) {
		if(!validate(report)) {
			return;
		}
		ReportDTO dto = supUpReport(report);
		metric.setLastAddedLines(dto.addedLines);
		metric.setLastNewLines(dto.addedLines);
		metric.setLastNewLinesPerDeveloper(getNewLinesPerDeveloper(dto.addedLines, metric.getProject()));
		metric.setLastDeletedLines(dto.deletedLines);
		updateAverage(metric, allMetrics);
		updateTotalAverage(metric, allMetrics);
		updateDifferenceNewLastInLastWeek(metric, allMetrics);
		updateAverage7DaysNewLines(metric, allMetrics);
	}
	
	private void updateAverage7DaysNewLines(Metric metric, List<Metric> allMetrics) {
		Integer newLinesInLastWeek = filterMetricsFormLastWeek(metric, allMetrics)
				.stream()
				.map(temp -> temp.getLastNewLines())
				.reduce(0, (sum, item) -> sum+item);
		BigDecimal average = new BigDecimal(newLinesInLastWeek)
				.divide(new BigDecimal(5), MathContext.DECIMAL32)
				.setScale(2, RoundingMode.HALF_UP);
		metric.setAvgNewLines7d(average);
	}

	private void updateDifferenceNewLastInLastWeek(Metric metric, List<Metric> allMetrics) {
		Integer newLinesInLastWeek = filterMetricsFormLastWeek(metric, allMetrics)
			.stream()
			.map(temp -> temp.getLastNewLines())
			.reduce(0, (sum, item) -> sum+item);
		metric.setDiffFromPreviousWeekNewLines(new BigDecimal(newLinesInLastWeek));
		
	}

	private List<Metric> filterMetricsFormLastWeek(Metric metric, List<Metric> allMetrics) {
		List<Metric> projectMetrics = filterProjectMetrics(metric, allMetrics);
		Date cutoff = getCutOfDate(metric);
		return projectMetrics.stream()
				.filter(temp -> temp.getCreated().after(cutoff))
				.collect(Collectors.toList());
	}

	private Date getCutOfDate(Metric metric) {
		GregorianCalendar cutoff = new GregorianCalendar();
		cutoff.setTime(metric.getCreated());
		cutoff.add(GregorianCalendar.DAY_OF_MONTH, -7);
		return cutoff.getTime();
	}

	private int getNewLinesPerDeveloper(int addedLines, Project project) {
		return Optional.ofNullable(project)
				.map(Project::getDevelopers)
				.map(developers -> new BigDecimal(addedLines)
						.divide(new BigDecimal(developers), MathContext.DECIMAL32)
						.setScale(2, RoundingMode.HALF_UP))
				.map(BigDecimal::intValue)
				.orElse(0);
	}

	private void updateTotalAverage(Metric metric, List<Metric> allMetrics) {
		List<Metric> metrics = new ArrayList<Metric>(allMetrics);
		metrics.add(metric);
		BigDecimal totalAverageNewLines = getTotalAverageNewLines(metrics);
		metric.setTotalAverageAddedLines(totalAverageNewLines);
		metric.setTotalAverageNewLines(totalAverageNewLines);
		metric.setTotalAverageDeletedLines(getTotalAverageDeletedLines(metrics));
	}

	private BigDecimal getTotalAverageDeletedLines(List<Metric> allMetrics) {
		return allMetrics.isEmpty()?
				BigDecimal.ZERO:
				sumUpDeletedLines(allMetrics)
					.divide(new BigDecimal(allMetrics.size()), MathContext.DECIMAL32)
					.divide(getAllDevelopers(allMetrics), MathContext.DECIMAL32)
					.setScale(2, RoundingMode.HALF_UP);
	}

	private BigDecimal getTotalAverageNewLines(List<Metric> allMetrics) {
		return allMetrics.isEmpty()?
				BigDecimal.ZERO:
				sumUpNewLines(allMetrics)
					.divide(new BigDecimal(allMetrics.size()), MathContext.DECIMAL32)
					.divide(getAllDevelopers(allMetrics), MathContext.DECIMAL32)
					.setScale(2, RoundingMode.HALF_UP);
	}

	private BigDecimal getAllDevelopers(List<Metric> allMetrics) {
		return new BigDecimal(
				allMetrics.stream()
					.map(Metric::getProject)
					.collect(Collectors.toSet())
					.stream()
					.map(Project::getDevelopers)
					.filter(developers -> developers!=null)
					.reduce(0, (sum, item) -> sum + item)
				);
		
	}

	private void updateAverage(Metric metric, List<Metric> allMetrics) {
		List<Metric> projectMetrics = filterProjectMetrics(metric, allMetrics);
		BigDecimal avgAddedLines = getAverageNewLines(projectMetrics, metric.getProject());
		metric.setAverageAddedLines(avgAddedLines);
		metric.setAverageNewLines(avgAddedLines);
		metric.setAverageDeletedLines(getAverageDeletedLines(projectMetrics, metric.getProject()));
	}

	private BigDecimal getAverageDeletedLines(List<Metric> projectMetrics, Project project) {
		return getDevelopers(project).equals(BigDecimal.ZERO) || projectMetrics.isEmpty()?
				BigDecimal.ZERO:
				sumUpDeletedLines(projectMetrics)
					.divide(new BigDecimal(projectMetrics.size()), MathContext.DECIMAL32)
					.divide(getDevelopers(project), MathContext.DECIMAL32)
					.setScale(2, RoundingMode.HALF_UP);
	}

	private BigDecimal sumUpDeletedLines(List<Metric> projectMetrics) {
		return new BigDecimal(projectMetrics.stream()
				.map(Metric::getLastDeletedLines)
				.filter(newLines -> newLines!=null)
				.reduce(0, (sum, item) -> sum + item)
				);
	}

	private BigDecimal getAverageNewLines(List<Metric> projectMetrics, Project project) {
		return getDevelopers(project).equals(BigDecimal.ZERO) || projectMetrics.isEmpty()?
				BigDecimal.ZERO:
				sumUpNewLines(projectMetrics)
					.divide(new BigDecimal(projectMetrics.size()), MathContext.DECIMAL32)
					.divide(getDevelopers(project), MathContext.DECIMAL32)
					.setScale(2, RoundingMode.HALF_UP);
	}

	private BigDecimal getDevelopers(Project project) {
		return Optional.ofNullable(project)
				.map(Project::getDevelopers)
				.map(BigDecimal::new)
				.orElse(BigDecimal.ZERO);
	}

	private BigDecimal sumUpNewLines(List<Metric> projectMetrics) {
		return new BigDecimal(projectMetrics.stream()
					.map(Metric::getLastAddedLines)
					.filter(newLines -> newLines!=null)
					.reduce(0, (sum, item) -> sum + item)
					);
	}

	private List<Metric> filterProjectMetrics(Metric metric, List<Metric> allMetrics) {
		List<Metric> list = allMetrics.stream()
				.filter(temp -> isBelongToProject(temp, metric.getProject()))
				.collect(Collectors.toList());
		list.add(metric);
		return list;
				
	}
	
	private boolean isBelongToProject(Metric metric, Project project) {
		return Optional.ofNullable(metric)
				.map(Metric::getProject)
				.map(Project::getHash)
				.map(hash -> hash.equals(project.getHash()))
				.orElse(false);
	}

	private ReportDTO supUpReport(TestResult report) {
		return Optional.ofNullable(report)
				.map(TestResult::getResult)
				.map(result -> result.split("\n"))
				.map(Arrays::asList)
				.orElseGet(() -> Collections.emptyList())
				.stream()
				.map(String::trim)
				.map(this::mapToReportDTO)
				.reduce(new ReportDTO(), ReportDTO::add);
	}
	
	private ReportDTO mapToReportDTO(String line) {
		String onlyLinesNumbers=prepareReportLine(line);
		ReportDTO dto = new ReportDTO();
		dto.addedLines = getAddedLines(onlyLinesNumbers);
		dto.deletedLines = getDeletedLines(onlyLinesNumbers);
		return dto;
	}

	private String prepareReportLine(String line) {
		String replaced = line.replaceAll("\\s+", WHITE_CHAR_MARK);
		return replaced.substring(0, replaced.lastIndexOf(WHITE_CHAR_MARK) + WHITE_CHAR_MARK.length());
	}

	private int getDeletedLines(String onlyLines) {
		return Optional.ofNullable(onlyLines)
				.map(line -> line.substring(line.indexOf(WHITE_CHAR_MARK)))
				.map(l -> l.substring(0, l.lastIndexOf(WHITE_CHAR_MARK)))
				.map(temp -> temp.replaceAll("\\"+WHITE_CHAR_MARK, ""))
				.map(String::trim)
				.map(BigDecimal::new)
				.map(BigDecimal::intValue)
				.orElse(0);
	}

	private int getAddedLines(String onlyLines) {
		return Optional.ofNullable(onlyLines)
				.map(line -> line.substring(0, line.indexOf(WHITE_CHAR_MARK)))
				.map(BigDecimal::new)
				.map(BigDecimal::intValue)
				.orElse(0);
	}

	class ReportDTO {
		private int addedLines;
		private int deletedLines;
		
		public ReportDTO add(ReportDTO that) {
			this.addedLines += that.addedLines;
			this.deletedLines += that.deletedLines;
			return this;
		}
	}
}
