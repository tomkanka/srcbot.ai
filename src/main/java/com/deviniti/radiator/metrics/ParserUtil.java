package com.deviniti.radiator.metrics;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;

public class ParserUtil {

	public Metric getLastMetric(List<Metric> allMetrics, Project project) {
		Metric last = null;
		for(Metric metric: allMetrics) {
			if(isMetricOfProject(metric, project)) {
				if(last == null) {
					last = metric;
				}else if(last.getCreated().before(metric.getCreated())) {
					last = metric;
				}
			}
		}
		return last;
	}
	
	public List<Metric> getAllMetricsOfProject(List<Metric> allMetrics, Project project) {
		return allMetrics.stream()
				.filter(metric -> isMetricOfProject(metric, project))
				.collect(Collectors.toList());
		
	}

	private boolean isMetricOfProject(Metric metric, Project project) {
		return Optional.ofNullable(metric)
				.map(Metric::getProject)
				.map(Project::getHash)
				.map(hash -> hash.equals(project.getHash()))
				.orElse(false);
	}

	
}
