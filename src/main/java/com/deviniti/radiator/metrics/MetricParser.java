package com.deviniti.radiator.metrics;

import java.util.List;

import org.springframework.stereotype.Component;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.TestResult;

@Component
public class MetricParser {
	private GeneralParser generalParser;
	private ClocParser clocParser;
	private TestJUnitParser testCoverageParser;
	private JacocoParser jacocoParser;
	private GitLinesParser gitLinesParser;
	
	public MetricParser() {
		generalParser = new GeneralParser();
		clocParser = new ClocParser();
		testCoverageParser = new TestJUnitParser();
		jacocoParser = new JacocoParser();
		gitLinesParser = new GitLinesParser();
	}

	public Metric parseToMetrics(List<TestResult> reports, List<Metric> allMetrics) {
		Metric metric = new Metric();
		updateProject(metric, reports);
		if(!reports.isEmpty()) {
			generalParser.updateMetric(reports.get(0), metric, allMetrics);
		}
		reports.forEach(report -> {
			switch(report.getType()) {
			case CLOC:
				clocParser.updateMetric(report, metric, allMetrics);
				break;
			case JUNIT_TESTS:
				testCoverageParser.updateMetric(report, metric, allMetrics);
				break;
			case JACOCO:
				jacocoParser.updateMetric(report, metric, allMetrics);
				break;
			case GIT_LINES:
				gitLinesParser.updateMetric(report, metric, allMetrics);
			}
		});		
		
		return metric;
	}

	private void updateProject(Metric metric, List<TestResult> reports) {
		metric.setProject(reports.stream()
				.filter(report -> report.getProject() != null)
				.findFirst()
				.map(TestResult::getProject)
				.orElse(null));
	}

}
