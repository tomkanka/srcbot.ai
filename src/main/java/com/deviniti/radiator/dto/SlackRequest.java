package com.deviniti.radiator.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SlackRequest {
	
	@JsonProperty("text")
	private String message;
	@JsonProperty("blocks")
	List<SlackRequestBlock> blocks;
	
	public SlackRequest(String message) {
		this.message = message;
		this.blocks = null;
	}
	
	public SlackRequest(List<SlackRequestBlock> blocks) {
		this.message = null;
		this.blocks = blocks;
	}
	
	public SlackRequest addBlock(SlackRequestBlock block) {
		this.blocks.add(block);
		return this;
	}

	public String getMessage() {
		return message;
	}

	public SlackRequest setMessage(String message) {
		this.message = message;
		return this;
	}

	public List<SlackRequestBlock> getBlocks() {
		return blocks;
	}

	public void setBlocks(List<SlackRequestBlock> blocks) {
		this.blocks = blocks;
	}

	
}
