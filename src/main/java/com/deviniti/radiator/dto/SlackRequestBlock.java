package com.deviniti.radiator.dto;

import java.util.List;
import java.util.Random;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SlackRequestBlock {

	@JsonProperty("type")
	private String type;
	@JsonProperty("block_id")
	private String block_id;
	@JsonProperty("text")
	private SlackRequestField text;
	@JsonProperty("fields")
	private List<SlackRequestField> fields;
	
	public SlackRequestBlock(List<SlackRequestField> fields) {
		this.type = "section";
		this.block_id="section"+new Random(System.currentTimeMillis()).nextInt();
		this.text = null;
		this.fields = fields;
	}
	
	public SlackRequestBlock(String message) {
		this.type = "section";
		this.text = new SlackRequestField(message);
		this.fields = null;
	}
	
	public SlackRequestBlock addField(SlackRequestField field) {
		fields.add(field);
		return this;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<SlackRequestField> getFields() {
		return fields;
	}

	public void setFields(List<SlackRequestField> fields) {
		this.fields = fields;
	}

	public SlackRequestField getText() {
		return text;
	}

	public void setText(SlackRequestField text) {
		this.text = text;
	}

	public String getBlock_id() {
		return block_id;
	}

	public void setBlock_id(String block_id) {
		this.block_id = block_id;
	}

	
}
