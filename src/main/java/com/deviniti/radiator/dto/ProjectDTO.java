package com.deviniti.radiator.dto;

import com.deviniti.radiator.model.ProjectCloseStatus;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProjectDTO {
	
	private String name;
	private String hash;
	private String repository;
	private Integer developers;
	@JsonProperty("size_of_project")
	private Integer sizeOfProject;
	@JsonProperty("slack_web_hook_url")
	private String slackWebHookUrl;
	@JsonProperty("time_zone")
	private String timeZone;
	@JsonProperty("close_status")
	private ProjectCloseStatus closeStatus;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRepository() {
		return repository;
	}

	public void setRepository(String repository) {
		this.repository = repository;
	}

	public Integer getDevelopers() {
		return developers;
	}

	public void setDevelopers(Integer developers) {
		this.developers = developers;
	}

	public Integer getSizeOfProject() {
		return sizeOfProject;
	}

	public void setSizeOfProject(Integer sizeOfProject) {
		this.sizeOfProject = sizeOfProject;
	}

	public String getSlackWebHookUrl() {
		return slackWebHookUrl;
	}

	public void setSlackWebHookUrl(String slackWebHookUrl) {
		this.slackWebHookUrl = slackWebHookUrl;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public ProjectCloseStatus getCloseStatus() {
		return closeStatus;
	}

	public void setCloseStatus(ProjectCloseStatus closeStatus) {
		this.closeStatus = closeStatus;
	}

	
}
