package com.deviniti.radiator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RsaKeysDTO {

	@JsonProperty("public")
	private String publicKey;
	@JsonProperty("private")
	private String privateKey;
	
	
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	public String getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	
}
