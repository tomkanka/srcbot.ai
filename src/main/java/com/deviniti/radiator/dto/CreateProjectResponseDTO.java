package com.deviniti.radiator.dto;

public class CreateProjectResponseDTO {

	private String hash;
	private String publicKey;
	
	public CreateProjectResponseDTO(String hash, String publicKey) {
		this.hash = hash;
		this.publicKey = publicKey;
	}
	
	public String getHash() {
		return hash;
	}
	public String getPublicKey() {
		return publicKey;
	}
	
}
