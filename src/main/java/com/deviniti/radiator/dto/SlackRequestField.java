package com.deviniti.radiator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SlackRequestField {

	@JsonProperty("type")
	private String type;
	@JsonProperty("text")
	private String text;
	
	public SlackRequestField(String text) {
		this.type="mrkdwn";
		this.text = text;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	
}
