package com.deviniti.radiator.dto;

import java.math.BigDecimal;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SimpleMetricsDTO {

	@JsonProperty("total_number_of_lines")
	private Integer totalLinnes;
	@JsonProperty("total_unit_tests")
	private Integer unitTests;
	@JsonProperty("total_critical_and_high_errors")
	private Integer heighErrors;
	@JsonProperty("last_days")
	private Integer lastDays;
	@JsonProperty("last_new_lines")
	private Integer lastNewLines;
	@JsonProperty("last_modify_lines")
	private Integer lastModifiedLines;
	@JsonProperty("last_deleted_lines")
	private Integer lastDeletedLines;
	@JsonProperty("last_new_unit_tests")
	private Integer lastUnitTests;
	@JsonProperty("last_change_critical_and_high_errors")
	private Integer lastHeighErrorsChange;
	@JsonProperty("avg_new_lines")
	private BigDecimal averageNewLines;
	@JsonProperty("avg_modify_lines")
	private BigDecimal averageModifiedLines;
	@JsonProperty("avg_deleted_lines")
	private BigDecimal averageDeletedLines;
	@JsonProperty("avg_new_unit_tests")
	private BigDecimal averageNewUnitTests;
	@JsonProperty("avg_change_critical_and_high_errors")
	private BigDecimal averageHeighErrorsChange;
	@JsonProperty("total_avg_new_lines")
	private BigDecimal totalAverageNewLines;
	@JsonProperty("total_avg_modify_lines")
	private BigDecimal totalAverageModifiedLines;
	@JsonProperty("total_avg_deleted_lines")
	private BigDecimal totalAverageDeletedLines;
	@JsonProperty("total_avg_new_unit_tests")
	private BigDecimal totalAverageNewUnitTests;
	@JsonProperty("total_avg_change_critical_and_high_errors")
	private BigDecimal totalAverageHeighErrorsChange;
	@JsonProperty("days_to_finish")
	private Integer daysToFinish;
}
