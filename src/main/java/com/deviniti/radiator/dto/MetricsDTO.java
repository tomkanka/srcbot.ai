package com.deviniti.radiator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetricsDTO {

	@JsonProperty("simple_metrics")
	private SimpleMetricsDTO simpleMetrics;
	@JsonProperty("git_metrics")
	private GitMetricsDTO gitMetrics;

	public SimpleMetricsDTO getSimpleMetrics() {
		return simpleMetrics;
	}
	public void setSimpleMetrics(SimpleMetricsDTO simpleMetrics) {
		this.simpleMetrics = simpleMetrics;
	}
	public GitMetricsDTO getGitMetrics() {
		return gitMetrics;
	}
	public void setGitMetrics(GitMetricsDTO gitMetrics) {
		this.gitMetrics = gitMetrics;
	}
	
}
