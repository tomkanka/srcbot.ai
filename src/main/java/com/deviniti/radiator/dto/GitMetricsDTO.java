package com.deviniti.radiator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GitMetricsDTO {

	@JsonProperty("total_commits")
	private int commitsNumber;

	public int getCommitsNumber() {
		return commitsNumber;
	}
	public void setCommitsNumber(int commitsNumber) {
		this.commitsNumber = commitsNumber;
	}
	
	
}
