package com.deviniti.radiator.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
class ExceptionProcessor {

	@Pointcut("within(com.deviniti.radiator.controller..*)")
	public void anyController() {
	}
	
	@Pointcut("execution (public * *(..))")
	public void publicMethod() {
	}
	
	@Before("anyController() && publicMethod()")
	public void onException(JoinPoint joinPoint) {
		System.out.println(joinPoint.getTarget().getClass());
		System.out.println(joinPoint.getSignature().getName());
	}
}
