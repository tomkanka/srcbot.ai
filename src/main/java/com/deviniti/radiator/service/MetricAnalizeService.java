package com.deviniti.radiator.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deviniti.radiator.metrics.MetricParser;
import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.TestResult;
import com.deviniti.radiator.repository.MetricsRepository;
import com.deviniti.radiator.repository.TestResultRepository;

@Service
@Transactional
public class MetricAnalizeService {

	@Autowired
	private TestResultRepository testResultRepository;
	@Autowired 
	private MetricsRepository metricsRepository;
	@Autowired
	private MetricParser metricParser;
	
	public void calculateMetrics() {
		List<TestResult> testResults = testResultRepository.findByCreatedGreaterThanOrderByCreatedDesc(getToday());
		List<Metric> allMetrics = metricsRepository.findAll();
		Map<String, List<TestResult>> mapTestResults = mapTestResults(testResults);
		mapTestResults = removeTestResultForCreatedMetrics(mapTestResults, allMetrics);
		mapTestResults.forEach((key, value) -> calculateMetric(value, allMetrics));
	}

	private void calculateMetric(List<TestResult> value, List<Metric> allMetrics) {
		try {
			Metric metric = metricParser.parseToMetrics(value, allMetrics);
			metricsRepository.save(metric);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Map<String, List<TestResult>> removeTestResultForCreatedMetrics(Map<String, List<TestResult>> mapTestResults, List<Metric> allMetrics) {
		Map<String, List<TestResult>> result = new HashMap<String, List<TestResult>>();
		for(String project: mapTestResults.keySet()) {
			if(!isCreatedMetric(allMetrics, mapTestResults.get(project), project)) {
				result.put(project, mapTestResults.get(project));
			}
		}
		return result;
	}

	private boolean isCreatedMetric(List<Metric> allMetrics, List<TestResult> list, String projectHash) {
		if(list.isEmpty()) {
			return true;
		}
		
		Date testResultCreated = list.get(0).getCreated();
		return allMetrics.stream()
				.filter(metric -> metric.getProject().getHash() != null)
				.filter(metric -> metric.getProject().getHash().equals(projectHash))
				.filter(metric -> metric.getCreated()!=null)
				.filter(metric -> metric.getCreated().after(testResultCreated))
				.findFirst()
				.isPresent() ;
				
	}

	private Map<String, List<TestResult>> mapTestResults(List<TestResult> testResults) {
		Map<String, List<TestResult>> map = new HashMap<String, List<TestResult>>();
		testResults.forEach(testResult -> {
			List<TestResult> list = map.get(testResult.getProject());
			if(list == null) {
				list = new ArrayList<TestResult>();
				map.put(testResult.getProject().getHash(), list);
			}
			boolean canAdd = list.stream()
					.filter(test -> test.getType().equals(testResult.getType()))
					.findFirst()
					.isEmpty();
			if(canAdd) {
				list.add(testResult);
			}
		});
		return map;
	}

	private Date getToday() {
		GregorianCalendar today = new GregorianCalendar();
		today.set(GregorianCalendar.HOUR, 0);
		today.set(GregorianCalendar.MINUTE, 0);
		today.set(GregorianCalendar.MILLISECOND, 0);
		today.set(GregorianCalendar.MILLISECOND, 0);
		return today.getTime();
	}
	
}
