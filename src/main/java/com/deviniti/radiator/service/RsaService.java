package com.deviniti.radiator.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deviniti.radiator.dto.RsaKeysDTO;
import com.deviniti.radiator.exception.ExceptionCode;
import com.deviniti.radiator.exception.RadiatorException;
import com.deviniti.radiator.repository.ProjectRepository;

@Service
public class RsaService {
	
	@Autowired
	private ProjectRepository projectRepository;

	public RsaKeysDTO generateRsaKeys(String projectHash) {
		KeyPair kp = generateRsaKeys();
		RsaKeysDTO rsaKeys = mapToDto(kp);
		updatePorjectWithRsaKeys(projectHash, rsaKeys);
		return rsaKeys;
	}

	public KeyPair generateRsaKeys() {
		try {
			KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(2048);
			return kpg.generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			throw new RadiatorException(ExceptionCode.GENERATE_RSA_KEY);
		}
	}
	
	private RsaKeysDTO mapToDto(KeyPair kp) {
		RsaKeysDTO dto = new RsaKeysDTO();
		Key publicKey = kp.getPublic();
		Key privateKey = kp.getPrivate();
		dto.setPublicKey(encodePublicKey(publicKey));
		dto.setPrivateKey(encodePrivateKey(privateKey));
		return dto;
	}

	private String encodePrivateKey(Key privateKey) {

		
		return Base64.getMimeEncoder().encodeToString(privateKey.getEncoded());
	}

	private String encodePublicKey(Key publickey) {
		try {
			byte [] pkey = encodePublicKey((RSAPublicKey) publickey);
			String encoded = new String(Base64.getEncoder().encode(pkey), "UTF-8");
			return "ssh-rsa " + encoded + " some@user" ;
		} catch (IOException e) {
			throw new RadiatorException(ExceptionCode.GENERATE_RSA_KEY);
		}
	}

	private String keyToString(Key key) {
		Base64.Encoder encoder = Base64.getEncoder();
		return encoder.encodeToString(key.getEncoded());
	}
	
	

    /**
     * @throws IOException 
     * @link https://stackoverflow.com/questions/3706177/how-to-generate-ssh-compatible-id-rsa-pub-from-java
     *
     * The key format used by ssh is defined in the RFC #4253. The format for RSA public key is the following :

     * string    "ssh-rsa"
     * mpint     e  // key public exponent
     * mpint     n  // key modulus
     *
     * All data type encoding is defined in the section #5 of RFC #4251. string and mpint (multiple precision integer) types are encoded this way :
     *
     * 4-bytes word: data length (unsigned big-endian 32 bits integer)
     * n bytes     : binary representation of the data
     *
     * or instance, the encoding of the string "ssh-rsa" is:
     *
     * byte[] data = new byte[] {0, 0, 0, 7, 's', 's', 'h', '-', 'r', 's', 'a'};
     */
    private static byte[] encodePublicKey(RSAPublicKey key) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        /* encode the "ssh-rsa" string */
        byte[] sshrsa = new byte[] {0, 0, 0, 7, 's', 's', 'h', '-', 'r', 's', 'a'};
        out.write(sshrsa);
        /* Encode the public exponent */
        BigInteger e = key.getPublicExponent();
        byte[] data = e.toByteArray();
        encodeUInt32(data.length, out);
        out.write(data);
        /* Encode the modulus */
        BigInteger m = key.getModulus();
        data = m.toByteArray();
        encodeUInt32(data.length, out);
        out.write(data);
        return out.toByteArray();
    }

    private static void encodeUInt32(int value, OutputStream out) throws IOException {
        byte[] tmp = new byte[4];
        tmp[0] = (byte)((value >>> 24) & 0xff);
        tmp[1] = (byte)((value >>> 16) & 0xff);
        tmp[2] = (byte)((value >>> 8) & 0xff);
        tmp[3] = (byte)(value & 0xff);
        out.write(tmp);
    }
    
    private void updatePorjectWithRsaKeys(String projectHash, RsaKeysDTO rsaKeys) {
		projectRepository.findByHash(projectHash).ifPresent(project -> {
			project.setPublicKey(rsaKeys.getPublicKey());
			project.setPrivateKey(rsaKeys.getPrivateKey());
		});
	}
}
