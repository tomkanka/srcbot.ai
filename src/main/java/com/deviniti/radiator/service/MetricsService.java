package com.deviniti.radiator.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deviniti.radiator.dto.MetricsDTO;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.repository.ProjectRepository;

@Service
@Transactional
public class MetricsService {

	@Autowired
	private ProjectRepository projectRepository;
	
	@Value("${metric.generate.hour}")
	private int hourOfGenerateMetric;
	
	public MetricsDTO generateSimpleMetrics(String hash) {
		//TODO do zaimplamentowania;
		return null;
	}

	public void selectProjectForCalculateMetrics() {
		projectRepository.findAll()
			.stream()
			.filter(this::checkIsProjectToGenerateMetrics)
			.map(project -> {
				project.setGenerateMetricsFlag(true);
				return project;
			})
			.collect(Collectors.toList());
	}
	
	
	
	private boolean checkIsProjectToGenerateMetrics(Project project) {
		return Optional.ofNullable(project)
				.map(Project::getTimeZone)
				.flatMap(this::getCurrentTimeInLocalTimeZone)
				.map(LocalDateTime::getHour)
				.map(hour -> hour>=hourOfGenerateMetric)
				.orElse(false);
	}
	
	private Optional<LocalDateTime> getCurrentTimeInLocalTimeZone(String zoneId) {
		try {
		return Optional.ofNullable(zoneId)
				.map(ZoneId::of)
				.map(LocalDateTime::now);
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
