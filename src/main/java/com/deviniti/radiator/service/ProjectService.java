package com.deviniti.radiator.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.deviniti.radiator.dto.CreateProjectDTO;
import com.deviniti.radiator.dto.CreateProjectResponseDTO;
import com.deviniti.radiator.dto.ProjectDTO;
import com.deviniti.radiator.dto.RsaKeysDTO;
import com.deviniti.radiator.exception.ExceptionCode;
import com.deviniti.radiator.exception.RadiatorException;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.model.ProjectCloseStatus;
import com.deviniti.radiator.repository.ProjectRepository;

@Service
@Transactional
public class ProjectService {

	@Autowired
	private ProjectRepository projectRepository;
	@Autowired
	private SlackService slackService;
	@Autowired
	private RsaService rsaService;
	
	public CreateProjectResponseDTO create(CreateProjectDTO createProjectDTO) {
		ProjectDTO projectDTO = mapCreateDtoToDto(createProjectDTO);
		Project project = createProject(projectDTO);
		String publicKey = null;
		if (! projectDTO.getRepository().startsWith("http"))
			publicKey = generateRsaKeysForProject(projectDTO);
		slackService.sedWelcome(project);
		return new CreateProjectResponseDTO(project.getHash(), publicKey);
	}
	
	public void update(ProjectDTO projectDTO) {
		projectRepository.findByHash(projectDTO.getHash())
			.ifPresentOrElse(project -> mapToEntity(projectDTO, project.getId()), 
							() -> new RadiatorException(ExceptionCode.CANNOT_FIND_PROJECT_BY_HASH, projectDTO.getHash()));
	}
	
	public List<ProjectDTO> getAllProjects() {
		return projectRepository.findAll()
				.stream()
				.map(this::mapToDto)
				.collect(toList());
	}
	

	public ProjectDTO getProjectByHash(String hash) {
		return projectRepository.findByHash(hash)
				.map(this::mapToDto)
				.orElseThrow(() -> new RadiatorException(ExceptionCode.CANNOT_FIND_PROJECT_BY_HASH, hash));
	}

	public Project getProjectModelByHash(String hash) {
		return projectRepository.findByHash(hash)
				.orElseThrow(() -> new RadiatorException(ExceptionCode.CANNOT_FIND_PROJECT_BY_HASH, hash));
	}	
	
	public void closeProject(String hash, ProjectCloseStatus closeStatus) {
		projectRepository.findByHash(hash)
			.ifPresentOrElse(project -> project.setCloseStatus(closeStatus), 
							() -> new RadiatorException(ExceptionCode.CANNOT_FIND_PROJECT_BY_HASH, hash));
	}

	public void deleteProject(String hash) {
		projectRepository.deleteByHash(hash);
	}
	

	private Project mapToEntity(ProjectDTO projectDTO, Long id) {
		Project project = new Project();
		project.setId(id);
		project.setName(projectDTO.getName());
		project.setHash(projectDTO.getHash());
		project.setRepository(projectDTO.getRepository());
		project.setDevelopers(projectDTO.getDevelopers());
		project.setSizeOfProject(projectDTO.getSizeOfProject());
		project.setSlackWebHookUrl(projectDTO.getSlackWebHookUrl());
		project.setTimeZone(projectDTO.getTimeZone());
		return project;
	}

	private ProjectDTO mapToDto(Project project) {
		ProjectDTO dto = new ProjectDTO();
		dto.setName(project.getName());
		dto.setHash(project.getHash());
		dto.setRepository(project.getRepository());
		dto.setDevelopers(project.getDevelopers());
		dto.setSizeOfProject(project.getSizeOfProject());
		dto.setSlackWebHookUrl(project.getSlackWebHookUrl());
		dto.setTimeZone(project.getTimeZone());
		return dto;
	}

	
	private ProjectDTO mapCreateDtoToDto(CreateProjectDTO createProjectDTO) {
		ProjectDTO dto = new ProjectDTO();
		dto.setName(createProjectDTO.getName());
		dto.setRepository(createProjectDTO.getRepository());
		dto.setDevelopers(createProjectDTO.getDevelopers());
		dto.setSizeOfProject(createProjectDTO.getSizeOfProject());
		dto.setSlackWebHookUrl(createProjectDTO.getSlackWebHookUrl());
		dto.setTimeZone(createProjectDTO.getTimeZone());
		return dto;
	}	
	
	private Project createProject(ProjectDTO projectDTO) {
		String uuid = UUID.randomUUID().toString();
		projectDTO.setHash(uuid);
		Project project = mapToEntity(projectDTO, null);
		projectRepository.save(project);
		return project;
	}
	
	private String generateRsaKeysForProject(ProjectDTO projectDTO) {
		if(isGitRepositoryUrlBySsl(projectDTO)) {
			RsaKeysDTO rsaKeys = rsaService.generateRsaKeys(projectDTO.getHash());
			return rsaKeys.getPublicKey();
		}else {
			return null;
		}
	}

	private boolean isGitRepositoryUrlBySsl(ProjectDTO projectDTO) {
		return Optional.ofNullable(projectDTO)
				.map(ProjectDTO::getRepository)
				.map(repoUrl -> !repoUrl.contains("https:"))
				.orElse(false);
	}
}
