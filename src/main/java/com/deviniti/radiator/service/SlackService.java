package com.deviniti.radiator.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.deviniti.radiator.dto.SlackRequest;
import com.deviniti.radiator.metrics.MetricManager;
import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Notification;
import com.deviniti.radiator.model.Project;
import com.deviniti.radiator.repository.NotificationRepository;
import com.deviniti.radiator.repository.ProjectRepository;

@Service
@Transactional
public class SlackService {
	
	@Autowired
	private ProjectRepository projectRepository;
	@Autowired
	private NotificationRepository notificationRepository;
	@Autowired
	private MetricManager metricManager;
	@Value("${metric.send.hour}")
	private int hourOfSendMetrics;
	@Value("${notification.max_number_per_day}")
	private int maxNumberNotificationsPerDay;
	
	private Random random = new Random(System.currentTimeMillis()); 

	public void sendMetrics() {
		projectRepository.findAll()
			.stream()
			.filter(this::checkIsMetricToSend)
			.map(this::sendMetricsMessage)
			.collect(Collectors.toList());
	}
	
	public void sendNotification() {
		projectRepository.findAll()
			.stream()
			.filter(this::checkIsNotificationToSend)
			.map(this::sendNotificationMessage)
			.collect(Collectors.toList());
			
	}
	
	public void sedWelcome(Project project) {
		SlackRequest request = new SlackRequest(createWelcomeMessage());
		sendMessage(request, project.getSlackWebHookUrl());
	}
	
	private String createWelcomeMessage() {
		return "Hello world! You beautifull minds:kiss::nerd_face::hearts:\nI am SourceBot :robot_face:\nand I will watch over the development of your project :rocket::rocket::rocket:";
	}

	private boolean checkIsMetricToSend(Project project) {
		return true;
		//return isTimeToSendMetrics(project) && !isMetricSended(project);
	}

	private boolean isTimeToSendMetrics(Project project) {
		return getCurrentTimeInLocalTimeZone(project.getTimeZone())
				.map(LocalDateTime::getHour)
				.map(hour -> hour>=hourOfSendMetrics)
				.orElse(false);
	}

	private boolean isMetricSended(Project project) {
		return Optional.ofNullable(project)
				.map(Project::getLastMetric)
				.map(Metric::isSended)
				.orElse(true);
	}
	
	private Optional<LocalDateTime> getCurrentTimeInLocalTimeZone(String zoneId) {
		return Optional.ofNullable(zoneId)
				.map(ZoneId::of)
				.map(LocalDateTime::now);
	}
	
	private boolean sendMetricsMessage(Project project) {
		if (project.getSlackWebHookUrl() == null)
			return false;
		
		SlackRequest request = metricManager.prepareSummaryMessage(project);
		
		if (!StringUtils.hasText(request.getMessage()))
		{
			System.err.println("Empty metric message for project.id" + project.getId());
			return false;
		}
		
		boolean result = sendMessage(request, project.getSlackWebHookUrl());
		if(result) {
			//project.getLastMetric().setSended(true);
			//TODO
		}
		return result;
	}
	
	private boolean checkIsNotificationToSend(Project project) {
		return isTimeToSendNotification(project) && canSendNotification(project);
	}

	private boolean isTimeToSendNotification(Project project) {
		return Optional.ofNullable(project)
				.map(Project::getTimeZone)
				.flatMap(this::getCurrentTimeInLocalTimeZone)
				.map(LocalDateTime::getHour)
				.filter(hour -> hour >= 8)
				.filter(hour -> hour <= 23)
				.map(hour -> random.nextBoolean())
				.orElse(false);
	}

	private boolean canSendNotification(Project project) {
		Notification lastNotification = project.getLastNotification();
		if(lastNotification==null || isBefore(lastNotification.getCreated())) {
			lastNotification = createNewLastNotification(project);
		}
		return lastNotification.getSendedNumber() < maxNumberNotificationsPerDay;
	}

	private boolean isBefore(Date created) {
		if(created == null) {
			return true;
		}
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(created);
		GregorianCalendar now = new GregorianCalendar();
		boolean result = false;
		if(calendar.get(GregorianCalendar.YEAR) < now.get(GregorianCalendar.YEAR)) {
			result =  true;
		}else if(calendar.get(GregorianCalendar.YEAR) == now.get(GregorianCalendar.YEAR)) {
			if(calendar.get(GregorianCalendar.MONTH) < now.get(GregorianCalendar.MONTH)) {
				result =  true;
			}else if(calendar.get(GregorianCalendar.MONTH) == now.get(GregorianCalendar.MONTH)) {
				if(calendar.get(GregorianCalendar.DAY_OF_MONTH) < now.get(GregorianCalendar.DAY_OF_MONTH)) {
					result =  true;
				}
			}
		}
		return result;
	}

	private Notification createNewLastNotification(Project project) {
		Notification notification = new Notification();
		notification.setCreated(new Date());
		notification.setProject(project);
		notification.setSendedNumber(0);
		notificationRepository.save(notification);
		return notification;
	}

	private boolean sendNotificationMessage(Project project) {
		if (project.getSlackWebHookUrl() == null)
			return false;
		
		SlackRequest request = metricManager.prepareNotificationMessage(project);
		
		if (!StringUtils.hasText(request.getMessage()))
		{
			System.err.println("Empty metric message for project.id" + project.getId());
			return false;
		}
		
		boolean result = sendMessage(request, project.getSlackWebHookUrl());
		if(result) {
			Optional.ofNullable(project)
				.map(Project::getLastNotification)
				.ifPresent(notification -> notification.notificationSended());
		}
		return result;
	}
	
	
	private boolean sendMessage(SlackRequest request, String webHook) {
		RestTemplate restClient = new RestTemplate();
		ResponseEntity<String> response = restClient.postForEntity(webHook, request, String.class);
		return response.getStatusCode().equals(HttpStatus.OK);
	}
}
