package com.deviniti.radiator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deviniti.radiator.dto.ProjectDTO;
import com.deviniti.radiator.service.MetricAnalizeService;
import com.deviniti.radiator.service.MetricsService;
import com.deviniti.radiator.service.ProjectService;
import com.deviniti.radiator.service.SlackService;

@RestController
@RequestMapping("metrics")
public class MetricsController {

	@Autowired
	private MetricsService metricsService;
	@Autowired
	private SlackService slackService;
	@Autowired
	private ProjectService projectService;
	@Autowired
	private MetricAnalizeService metricAnalizeService;
	
	
	@Scheduled(cron = "${cron.metrics.calculate}")
	public void selectProjectForCalculateMetrics() {
		metricsService.selectProjectForCalculateMetrics();
	}
	
	@Scheduled(cron = "${cron.metrics.parse}")
	public void calculateMetrics() {
		metricAnalizeService.calculateMetrics();
	}
	
	@Scheduled(cron = "${cron.metrics.send}")
	public void sendMetricsToSlack() {
		slackService.sendMetrics();
	}
	
	@Scheduled(cron = "${cron.notification.send}")
	public void sendNotificationToSlack() {
		slackService.sendNotification();
	}
	

	

}
