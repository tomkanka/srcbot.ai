package com.deviniti.radiator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deviniti.radiator.dto.CreateProjectDTO;
import com.deviniti.radiator.dto.CreateProjectResponseDTO;
import com.deviniti.radiator.dto.MetricsDTO;
import com.deviniti.radiator.dto.ProjectDTO;
import com.deviniti.radiator.dto.RsaKeysDTO;
import com.deviniti.radiator.model.ProjectCloseStatus;
import com.deviniti.radiator.service.MetricsService;
import com.deviniti.radiator.service.ProjectService;
import com.deviniti.radiator.service.RsaService;

@RestController
@RequestMapping("project")
public class ProjectController {

	@Autowired
	private ProjectService projectService;
	@Autowired
	private RsaService rsaService;
	@Autowired
	private MetricsService metricsService;
	
	@PostMapping
	public CreateProjectResponseDTO saveNewProject(@RequestBody CreateProjectDTO projectDto) {
		return projectService.create(projectDto);
	}
	
	@PutMapping
	public void updateProject(@RequestBody ProjectDTO projectDTO) {
		projectService.update(projectDTO);
	}
	
//	@GetMapping("/all")
	public List<ProjectDTO> getAllProjects() {
		return projectService.getAllProjects();
	}
	
	@GetMapping("/{hash}")
	public ProjectDTO getProjectByHash(@PathVariable("hash") String hash) {
		return projectService.getProjectByHash(hash);
	}
	
	@PatchMapping("/{hash}")
	public void closeProject(@PathVariable("hash") String hash, @RequestBody ProjectCloseStatus closeStatus) {
		projectService.closeProject(hash, closeStatus);
	}
	
	@DeleteMapping("/{hash}")
	public void deleteProject(@PathVariable("hash") String hash) {
		projectService.deleteProject(hash);
	}
	
	@GetMapping("/{hash}/key")
	public RsaKeysDTO generateKey(@PathVariable("hash") String hash) {
		return rsaService.generateRsaKeys(hash);
	}

	@GetMapping("/state")
	public String[] getCloseState() {
		String[] states = new String[] {"SUCCESS","SUSPENDED","FAILED"};
		return states;
	}
	
	@GetMapping("/{hash}/stat")
	public MetricsDTO generateSimpleStats(@PathVariable("hash") String hash) {
		return metricsService.generateSimpleMetrics(hash);
	}
	
}
