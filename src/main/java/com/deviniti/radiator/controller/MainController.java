package com.deviniti.radiator.controller;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MainController {

	
	public static class VersionDTO
	{
		public String version;
		public String name;
		public Instant buildTime;
		public String licence;
	}
	
	@Autowired
	BuildProperties buildProperties;
	
	@GetMapping
	public VersionDTO getVersion()
	{
		VersionDTO v = new VersionDTO();
		v.version = buildProperties.getVersion();
		v.name =  "srcBot.ai";//buildProperties.getName();
		v.buildTime = buildProperties.getTime();
		v.licence = "TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION\r\n" + 
				"\r\n" + 
				"1. Definitions.\r\n" + 
				"\r\n" + 
				"\"License\" shall mean the terms and conditions for use, reproduction, and distribution as defined by Sections 1 through 9 of this document.\r\n" + 
				"\r\n" + 
				"\"Licensor\" shall mean the copyright owner or entity authorized by the copyright owner that is granting the License.\r\n" + 
				"\r\n" + 
				"\"Legal Entity\" shall mean the union of the acting entity and all other entities that control, are controlled by, or are under common control with that entity. For the purposes of this definition, \"control\" means (i) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty percent (50%) or more of the outstanding shares, or (iii) beneficial ownership of such entity.\r\n" + 
				"\r\n" + 
				"\"You\" (or \"Your\") shall mean an individual or Legal Entity exercising permissions granted by this License.\r\n" + 
				"\r\n" + 
				"\"Source\" form shall mean the preferred form for making modifications, including but not limited to software source code, documentation source, and configuration files.\r\n" + 
				"\r\n" + 
				"\"Object\" form shall mean any form resulting from mechanical transformation or translation of a Source form, including but not limited to compiled object code, generated documentation, and conversions to other media types.";
		return v;
	}

}
