package com.deviniti.radiator.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@SequenceGenerator(name="seq_gen", sequenceName="notification_seq", allocationSize=1, initialValue=1)
public class Notification extends BaseEntity{

	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	private Date created;
	private int sendedNumber;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="project_id")
	private Project project;


	public void notificationSended() {
		this.sendedNumber++;
	}
	
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public int getSendedNumber() {
		return sendedNumber;
	}
	public void setSendedNumber(int sendedNumber) {
		this.sendedNumber = sendedNumber;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	
}
