package com.deviniti.radiator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Type;

@Entity
@SequenceGenerator(name="seq_gen", sequenceName="summary_seq", allocationSize=1, initialValue=1)
public class Summary extends BaseEntity{

	@Type(type="text")
	private String paragraph;
	
	public String getParagraph() {
		return paragraph;
	}

	public void setParagraph(String paragraph) {
		this.paragraph = paragraph;
	}

	public String getParagraphType() {
		return paragraphType;
	}

	public void setParagraphType(String paragraphType) {
		this.paragraphType = paragraphType;
	}

	@Column(name="paragraph_type")
	private String paragraphType;	

	
}
