package com.deviniti.radiator.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@SequenceGenerator(name = "seq_gen", sequenceName="test_result_seq", allocationSize=1, initialValue=1)
public class TestResult extends BaseEntity{

		
	@CreationTimestamp
	private Date created;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="project_id")
	private Project project;
	
	@Column
	@Enumerated(EnumType.STRING)
	private TestResultType type;
	
	@Column(columnDefinition = "TEXT")
	private String result;
	
	@Column(nullable=true)
	private Boolean analised;
	
	@Transient
	private TestResult previous;

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public TestResultType getType() {
		return type;
	}

	public void setType(TestResultType type) {
		this.type = type;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Boolean getAnalised() {
		return analised;
	}

	public void setAnalised(Boolean analised) {
		this.analised = analised;
	}

	public TestResult getPrevious() {
		return previous;
	}

	public void setPrevious(TestResult previous) {
		this.previous = previous;
	} 
	
}
