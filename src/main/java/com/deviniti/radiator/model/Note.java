package com.deviniti.radiator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Type;

@Entity
@SequenceGenerator(name="seq_gen", sequenceName="note_seq", allocationSize=1, initialValue=1)
public class Note extends BaseEntity{

	@Type(type="text")
	private String content;

	@Column(name="note_type")
	private String paragraphType;	

	@Type(type="text")	
	@Column(name="logical_condition")
	private String logicalCondition;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getParagraphType() {
		return paragraphType;
	}

	public void setParagraphType(String paragraphType) {
		this.paragraphType = paragraphType;
	}

	public String getLogicalCondition() {
		return logicalCondition;
	}

	public void setLogicalCondition(String logicalCondition) {
		this.logicalCondition = logicalCondition;
	}	
	
}
