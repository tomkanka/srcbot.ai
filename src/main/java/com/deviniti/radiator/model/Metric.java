package com.deviniti.radiator.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@SequenceGenerator(name = "seq_gen", sequenceName="metric_seq", allocationSize=1, initialValue=1)
public class Metric extends BaseEntity{
	
	@CreationTimestamp
	private Date created;

	@Column(name="total_number_of_lines")
	private Integer totalLines;
	@Column(name="total_unit_tests")
	private Integer unitTests;
	@Column(name="total_critical_and_high_errors")
	private Integer heighErrors;
	@Column(name="coverage")
	private BigDecimal coverage;
	@Column(name="last_days")
	private Integer lastDays;
	@Column(name="last_new_lines")
	private Integer lastNewLines;
	@Column(name="last_new_lines_per_developer")
	private Integer lastNewLinesPerDeveloper;
	@Column(name="last_added_lines")
	private Integer lastAddedLines;
	@Column(name="last_modify_lines")
	private Integer lastModifiedLines;
	@Column(name="last_deleted_lines")
	private Integer lastDeletedLines;
	@Column(name="last_new_unit_tests")
	private Integer lastUnitTests;
	@Column(name="last_change_critical_and_high_errors")
	private Integer lastHeighErrorsChange;
	@Column(name="avg_new_lines")
	private BigDecimal averageNewLines;
	@Column(name="avg_added_lines")
	private BigDecimal averageAddedLines;
	@Column(name="avg_modify_lines")
	private BigDecimal averageModifiedLines;
	@Column(name="avg_deleted_lines")
	private BigDecimal averageDeletedLines;
	@Column(name="avg_new_unit_tests")
	private BigDecimal averageNewUnitTests;
	@Column(name="avg_change_critical_and_high_errors")
	private BigDecimal averageHeighErrorsChange;
	@Column(name="total_avg_new_lines")
	private BigDecimal totalAverageNewLines;
	@Column(name="total_avg_added_lines")
	private BigDecimal totalAverageAddedLines;
	@Column(name="total_avg_modify_lines")
	private BigDecimal totalAverageModifiedLines;
	@Column(name="total_avg_deleted_lines")
	private BigDecimal totalAverageDeletedLines;
	@Column(name="total_avg_new_unit_tests")
	private BigDecimal totalAverageNewUnitTests;
	@Column(name="total_avg_change_critical_and_high_errors")
	private BigDecimal totalAverageHeighErrorsChange;
	@Column(name="days_to_finish")
	private Integer daysToFinish;
	
	@Column(name="days_from_last_check")
	private Integer daysFromLastCheck;
	
	@Column(name="diff_from_previous_week_new_lines")
	private BigDecimal diffFromPreviousWeekNewLines;	
	
	@Column(name="diff_other_teams_new_lines")
	private BigDecimal diffOtherTeamsNewLines;	
	
	@Column(name="last_change_static_errors")
	private Integer lastStaticErrorsChange;
	
	@Column(name="total_static_errors")
	private Integer totalStaticErrorsChange;
	
	private boolean sended;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="project_id")
	private Project project;
	

	@Column(name="avg_new_lines_7d")
	private BigDecimal avgNewLines7d;
	
	@Column(name="avg_new_unit_tests_7d")
	private BigDecimal avgNewUnitTests7d;
	
	@Column(name="avg_static_errors_7d")
	private BigDecimal avgStaticErrors7d;
	
	public BigDecimal getDiffFromPreviousWeekNewLines() {
		return diffFromPreviousWeekNewLines;
	}
	public void setDiffFromPreviousWeekNewLines(BigDecimal diffFromPreviousWeekNewLines) {
		this.diffFromPreviousWeekNewLines = diffFromPreviousWeekNewLines;
	}
	public BigDecimal getDiffOtherTeamsNewLines() {
		return diffOtherTeamsNewLines;
	}
	public void setDiffOtherTeamsNewLines(BigDecimal diffOtherTeamsNewLines) {
		this.diffOtherTeamsNewLines = diffOtherTeamsNewLines;
	}

	public Integer getLastNewLinesPerDeveloper() {
		return lastNewLinesPerDeveloper;
	}
	public void setLastNewLinesPerDeveloper(Integer lastNewLinesPerDeveloper) {
		this.lastNewLinesPerDeveloper = lastNewLinesPerDeveloper;
	}
	public Integer getTotalLines() {
		return totalLines;
	}
	public void setTotalLines(Integer totalLines) {
		this.totalLines = totalLines;
	}
	public Integer getUnitTests() {
		return unitTests;
	}
	public void setUnitTests(Integer unitTests) {
		this.unitTests = unitTests;
	}
	public Integer getHeighErrors() {
		return heighErrors;
	}
	public void setHeighErrors(Integer heighErrors) {
		this.heighErrors = heighErrors;
	}
	public Integer getLastDays() {
		return lastDays;
	}
	public void setLastDays(Integer lastDays) {
		this.lastDays = lastDays;
	}
	public Integer getLastNewLines() {
		return lastNewLines;
	}
	public void setLastNewLines(Integer lastNewLines) {
		this.lastNewLines = lastNewLines;
	}
	public Integer getLastModifiedLines() {
		return lastModifiedLines;
	}
	public void setLastModifiedLines(Integer lastModifiedLines) {
		this.lastModifiedLines = lastModifiedLines;
	}
	public Integer getLastDeletedLines() {
		return lastDeletedLines;
	}
	public void setLastDeletedLines(Integer lastDeletedLines) {
		this.lastDeletedLines = lastDeletedLines;
	}
	public Integer getLastUnitTests() {
		return lastUnitTests;
	}
	public void setLastUnitTests(Integer lastUnitTests) {
		this.lastUnitTests = lastUnitTests;
	}
	public Integer getLastHeighErrorsChange() {
		return lastHeighErrorsChange;
	}
	public void setLastHeighErrorsChange(Integer lastHeighErrorsChange) {
		this.lastHeighErrorsChange = lastHeighErrorsChange;
	}
	public BigDecimal getAverageNewLines() {
		return averageNewLines;
	}
	public void setAverageNewLines(BigDecimal averageNewLines) {
		this.averageNewLines = averageNewLines;
	}
	public BigDecimal getAverageModifiedLines() {
		return averageModifiedLines;
	}
	public void setAverageModifiedLines(BigDecimal averageModifiedLines) {
		this.averageModifiedLines = averageModifiedLines;
	}
	public BigDecimal getAverageDeletedLines() {
		return averageDeletedLines;
	}
	public void setAverageDeletedLines(BigDecimal averageDeletedLines) {
		this.averageDeletedLines = averageDeletedLines;
	}
	public BigDecimal getAverageNewUnitTests() {
		return averageNewUnitTests;
	}
	public void setAverageNewUnitTests(BigDecimal averageNewUnitTests) {
		this.averageNewUnitTests = averageNewUnitTests;
	}
	public BigDecimal getAverageHeighErrorsChange() {
		return averageHeighErrorsChange;
	}
	public void setAverageHeighErrorsChange(BigDecimal averageHeighErrorsChange) {
		this.averageHeighErrorsChange = averageHeighErrorsChange;
	}
	public BigDecimal getTotalAverageNewLines() {
		return totalAverageNewLines;
	}
	public void setTotalAverageNewLines(BigDecimal totalAverageNewLines) {
		this.totalAverageNewLines = totalAverageNewLines;
	}
	public BigDecimal getTotalAverageModifiedLines() {
		return totalAverageModifiedLines;
	}
	public void setTotalAverageModifiedLines(BigDecimal totalAverageModifiedLines) {
		this.totalAverageModifiedLines = totalAverageModifiedLines;
	}
	public BigDecimal getTotalAverageDeletedLines() {
		return totalAverageDeletedLines;
	}
	public void setTotalAverageDeletedLines(BigDecimal totalAverageDeletedLines) {
		this.totalAverageDeletedLines = totalAverageDeletedLines;
	}
	public BigDecimal getTotalAverageNewUnitTests() {
		return totalAverageNewUnitTests;
	}
	public void setTotalAverageNewUnitTests(BigDecimal totalAverageNewUnitTests) {
		this.totalAverageNewUnitTests = totalAverageNewUnitTests;
	}
	public BigDecimal getTotalAverageHeighErrorsChange() {
		return totalAverageHeighErrorsChange;
	}
	public void setTotalAverageHeighErrorsChange(BigDecimal totalAverageHeighErrorsChange) {
		this.totalAverageHeighErrorsChange = totalAverageHeighErrorsChange;
	}
	public Integer getDaysToFinish() {
		return daysToFinish;
	}
	public void setDaysToFinish(Integer daysToFinish) {
		this.daysToFinish = daysToFinish;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public boolean isSended() {
		return sended;
	}
	public void setSended(boolean sended) {
		this.sended = sended;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Integer getDaysFromLastCheck() {
		return daysFromLastCheck;
	}
	public void setDaysFromLastCheck(Integer daysFromLastCheck) {
		this.daysFromLastCheck = daysFromLastCheck;
	}
	public Integer getLastStaticErrorsChange() {
		return lastStaticErrorsChange;
	}
	public void setLastStaticErrorsChange(Integer lastStaticErrorsChange) {
		this.lastStaticErrorsChange = lastStaticErrorsChange;
	}
	public Integer getTotalStaticErrorsChange() {
		return totalStaticErrorsChange;
	}
	public void setTotalStaticErrorsChange(Integer totalStaticErrorsChange) {
		this.totalStaticErrorsChange = totalStaticErrorsChange;
	}
	public BigDecimal getCoverage() {
		return coverage;
	}
	public void setCoverage(BigDecimal coverage) {
		this.coverage = coverage;
	}
	public Integer getLastAddedLines() {
		return lastAddedLines;
	}
	public void setLastAddedLines(Integer lastAddedLines) {
		this.lastAddedLines = lastAddedLines;
	}
	public BigDecimal getAverageAddedLines() {
		return averageAddedLines;
	}
	public void setAverageAddedLines(BigDecimal averageAddedLines) {
		this.averageAddedLines = averageAddedLines;
	}
	public BigDecimal getTotalAverageAddedLines() {
		return totalAverageAddedLines;
	}
	public void setTotalAverageAddedLines(BigDecimal totalAverageAddedLines) {
		this.totalAverageAddedLines = totalAverageAddedLines;
	}
	
	public BigDecimal getAvgNewLines7d() {
		return avgNewLines7d;
	}
	public void setAvgNewLines7d(BigDecimal avgNewLines7d) {
		this.avgNewLines7d = avgNewLines7d;
	}
	public BigDecimal getAvgNewUnitTests7d() {
		return avgNewUnitTests7d;
	}
	public void setAvgNewUnitTests7d(BigDecimal avgNewUnitTests7d) {
		this.avgNewUnitTests7d = avgNewUnitTests7d;
	}
	public BigDecimal getAvgStaticErrors7d() {
		return avgStaticErrors7d;
	}
	public void setAvgStaticErrors7d(BigDecimal avgStaticErrors7d) {
		this.avgStaticErrors7d = avgStaticErrors7d;
	}

}
