package com.deviniti.radiator.model;

public enum ProjectCloseStatus {

	SUCCESS,
	SUSPENDED,
	FAILED
}
