package com.deviniti.radiator.model;

public enum TestResultType {

	CLOC,
	GIT_LINES,
	JUNIT_TESTS,
	FINDBUGS,
	JACOCO,
	
	MVN_TESTS,
	TEST_COVERAGE,
	OTHER
}
