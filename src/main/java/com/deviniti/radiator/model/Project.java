package com.deviniti.radiator.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.JoinFormula;
import org.hibernate.annotations.Type;

@Entity
@SequenceGenerator(name="seq_gen", sequenceName="project_seq", allocationSize=1, initialValue=1)
public class Project extends BaseEntity{

	@CreationTimestamp
	private Date created;
	
	private String hash;
	private String name;
	@Column(name="url")
	private String repository;
	private Integer developers;
	private Integer sizeOfProject;
	private String slackWebHookUrl;
	/** zone id */
	private String timeZone;
	@Enumerated(EnumType.STRING)
	private ProjectCloseStatus closeStatus;
	@Type(type="text")
	private String publicKey;
	@Type(type="text")
	private String privateKey;
	/** czy generowac statystyki */
	private boolean generateMetricsFlag;
	@ManyToOne
	@JoinFormula("(SELECT metric.id FROM metric WHERE metric.project_id = id ORDER BY metric.created DESC LIMIT 1)")
	private Metric lastMetric;
	@ManyToOne
	@JoinFormula("(SELECT notification.id FROM notification WHERE notification.project_id = id ORDER BY notification.created DESC LIMIT 1)")
	private Notification lastNotification;
	
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRepository() {
		return repository;
	}
	public void setRepository(String repository) {
		this.repository = repository;
	}
	public Integer getDevelopers() {
		return developers;
	}
	public void setDevelopers(Integer developers) {
		this.developers = developers;
	}
	public Integer getSizeOfProject() {
		return sizeOfProject;
	}
	public void setSizeOfProject(Integer sizeOfProject) {
		this.sizeOfProject = sizeOfProject;
	}
	public String getSlackWebHookUrl() {
		return slackWebHookUrl;
	}
	public void setSlackWebHookUrl(String slackWebHookUrl) {
		this.slackWebHookUrl = slackWebHookUrl;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public ProjectCloseStatus getCloseStatus() {
		return closeStatus;
	}
	public void setCloseStatus(ProjectCloseStatus closeStatus) {
		this.closeStatus = closeStatus;
	}
	public boolean isGenerateMetricsFlag() {
		return generateMetricsFlag;
	}
	public void setGenerateMetricsFlag(boolean generateMetricsFlag) {
		this.generateMetricsFlag = generateMetricsFlag;
	}
	public Metric getLastMetric() {
		return lastMetric;
	}
	public void setLastMetric(Metric lastMetric) {
		this.lastMetric = lastMetric;
	}
	public Notification getLastNotification() {
		return lastNotification;
	}
	public void setLastNotification(Notification lastNotification) {
		this.lastNotification = lastNotification;
	}
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	public String getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(closeStatus, created, developers, generateMetricsFlag, hash, lastMetric, lastNotification,
				name, privateKey, publicKey, repository, sizeOfProject, slackWebHookUrl, timeZone);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		return closeStatus == other.closeStatus && Objects.equals(created, other.created)
				&& Objects.equals(developers, other.developers) && generateMetricsFlag == other.generateMetricsFlag
				&& Objects.equals(hash, other.hash) && Objects.equals(lastMetric, other.lastMetric)
				&& Objects.equals(lastNotification, other.lastNotification) && Objects.equals(name, other.name)
				&& Objects.equals(privateKey, other.privateKey) && Objects.equals(publicKey, other.publicKey)
				&& Objects.equals(repository, other.repository) && Objects.equals(sizeOfProject, other.sizeOfProject)
				&& Objects.equals(slackWebHookUrl, other.slackWebHookUrl) && Objects.equals(timeZone, other.timeZone);
	}

	
	
}
