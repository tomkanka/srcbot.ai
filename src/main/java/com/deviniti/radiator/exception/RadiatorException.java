package com.deviniti.radiator.exception;

@SuppressWarnings("serial")
public class RadiatorException extends RuntimeException{

	private int code;
	
	public RadiatorException(ExceptionCode exception, String... args) {
		super(String.format(exception.getMessage(), args));
		this.code = exception.getCode();
	}

	public int getCode() {
		return code;
	}
	
}
