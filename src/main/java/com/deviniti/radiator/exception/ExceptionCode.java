package com.deviniti.radiator.exception;

public enum ExceptionCode {

	SAVE_NEW_PROJECT(1, "Problem with save new project"),
	CANNOT_FIND_PROJECT_BY_HASH(2, "Cannot find project with hash: %s"),
	GENERATE_RSA_KEY(3, "Cannot generate rsa key"),
	
	
	;
	private int code;
	private String message;
	
	private ExceptionCode(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
	
	
}
