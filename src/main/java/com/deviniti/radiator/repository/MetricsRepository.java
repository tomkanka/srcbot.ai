package com.deviniti.radiator.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.deviniti.radiator.model.Metric;
import com.deviniti.radiator.model.Project;

@Repository
public interface MetricsRepository extends JpaRepository<Metric, Long>{
	
	List<Metric> findByProjectAndSendedOrderByCreatedDesc(Project project,Boolean sended);

}
