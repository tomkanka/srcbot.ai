package com.deviniti.radiator.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deviniti.radiator.model.Project;

public interface ProjectRepository extends JpaRepository<Project, Long> {

	Optional<Project> findByHash(String hash);

	void deleteByHash(String hash);

}
