package com.deviniti.radiator.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.deviniti.radiator.model.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long>{

	@Query(value = "SELECT * FROM note n WHERE n.note_type=?1 ORDER BY RANDOM() LIMIT 1", nativeQuery = true)
	Note findRandomNoteByType(String noteType);

	@Query(value = "SELECT * FROM note n WHERE (n.note_type<>'JOKE') AND (n.logical_condition IS NOT NULL) ORDER BY RANDOM() LIMIT 10", nativeQuery = true)
	List<Note> findRandomNoteNotJoke();
	
}
