package com.deviniti.radiator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.deviniti.radiator.model.Summary;

@Repository
public interface SummaryRepository extends JpaRepository<Summary, Long>{

	@Query(value = "SELECT * FROM summary s WHERE s.paragraph_type=?1 ORDER BY RANDOM() LIMIT 1", nativeQuery = true)
	Summary findRandomPartSummary(String paragraphType);
	
	
}
