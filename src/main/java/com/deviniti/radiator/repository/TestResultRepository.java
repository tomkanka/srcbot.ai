package com.deviniti.radiator.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.deviniti.radiator.model.TestResult;

@Repository
public interface TestResultRepository extends JpaRepository<TestResult, Long>{

	public List<TestResult> findByCreatedGreaterThanOrderByCreatedDesc(Date date);
}
