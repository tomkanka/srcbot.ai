package com.deviniti.radiator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableCaching
@ComponentScan(basePackages = { "com.deviniti.radiator" })
@EntityScan(basePackages = { "com.deviniti.radiator.model" })
@EnableJpaRepositories(basePackages = { "com.deviniti.radiator.repository" })
public class MainApp {


	  public static void main(String[] args) {
	    SpringApplication.run(MainApp.class, args);
		  
//		  ZoneId.getAvailableZoneIds()
//		  .forEach(zodeId -> System.out.println(zodeId));
	  }
	  
}
