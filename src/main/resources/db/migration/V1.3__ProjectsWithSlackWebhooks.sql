insert into project (developers, hash, name, url, size_of_project, slack_web_hook_url, time_zone, id, generate_metrics_flag, created) 
values (5, '3df770e4-a51d-499a-934a-0a353c10553c', 'desk4u', 'https://github.com/sonya0504/desk4u.git', 12, 'https://hooks.slack.com/services/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', 'Europe/Warsaw', nextval('project_seq'), false, now());


insert into project (developers, hash, name, url, size_of_project, slack_web_hook_url, time_zone, id, generate_metrics_flag, created) 
values (10, '6ff156c7-3125-4686-bf9a-aebe6a1a1b11', 'sprawnik', 'https://wrosoftware@bitbucket.org/jarczak-sonia/sprawnik.git', 24, 'https://hooks.slack.com/services/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', 'America/Chicago', nextval('project_seq'), false, now());

insert into project (developers, hash, name, url, size_of_project, slack_web_hook_url, time_zone, id, generate_metrics_flag, created) 
values (25, '54ac5cd6-22a6-4576-a9a0-27e23be603dd', 'learn_react', 'https://github.com/sonya0504/learn_react.git', 4, 'https://hooks.slack.com/services/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', 'Asia/Tokyo', nextval('project_seq'), false, now());
